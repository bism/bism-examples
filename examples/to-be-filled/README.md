# How to fill

Assuming you have a jar you want to instrument named **my-app.jar** you have to :

1. Put it into *app* folder
2. Change *build.xml* `app.name` property to `my-app.jar`
3. Choose your configuration settings (especially `scope`) in *bism.xml*
4. Launch `ant run`

You are free to modify the default transformer into `transformer/FillMe.java`.
