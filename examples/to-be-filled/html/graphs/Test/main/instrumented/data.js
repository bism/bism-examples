var nodes = [];
var edges = [];
nodes.push({  color: { background: "white", border: "black" },    margin: { top: -1, right: 5, bottom: 15, left: 5 } ,  font: { multi: "html", size :14 , 
	 		ital: {
              color: "#FF3300", mod:"normal",face: "monospace"
            }, 
            boldital:{ color: "#00aa00", mod:"normal", face: "monospace"} ,
               bold:{ color: "#0000ff", mod:"bold",size :14, face: "monospace"} 
        }, id: 1, label: " B1 \n <code>L0\n<b><i> GETSTATIC java/lang/System.out : LPrintStream;\n<b><i> LDC '\nEntering  method: main'\n<b><i> INVOKEVIRTUAL PrintStream.print (Ljava/lang/String;)V\n<b><i> GETSTATIC java/lang/System.out : LPrintStream;\n<b><i> LDC '\nMy argument <my-arg-name> is :this is a sample argument'\n<b><i> INVOKEVIRTUAL PrintStream.print (Ljava/lang/String;)V\n<code> GETSTATIC java/lang/System.out : LPrintStream;\n<code> LDC 'Into main !'\n<b><i> GETSTATIC java/lang/System.out : LPrintStream;\n<b><i> LDC '\nBefore calling method: println'\n<b><i> INVOKEVIRTUAL PrintStream.print (Ljava/lang/String;)V\n<code> INVOKEVIRTUAL PrintStream.println (Ljava/lang/String;)V\n<b><i> GETSTATIC java/lang/System.out : LPrintStream;\n<b><i> LDC '\nAfter calling method: println'\n<b><i> INVOKEVIRTUAL PrintStream.print (Ljava/lang/String;)V\n<code>L1\n<b><i> GETSTATIC java/lang/System.out : LPrintStream;\n<b><i> LDC '\nExiting  method: main'\n<b><i> INVOKEVIRTUAL PrintStream.print (Ljava/lang/String;)V\n<code> RETURN\n<code>L2\n", title: "Start node" });
