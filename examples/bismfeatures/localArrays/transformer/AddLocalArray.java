import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.*;
import inria.bism.transformers.staticcontext.Method;

/*
 * This transformers shows how to create a local arraylist in a method and populate it with dynamic
 * context
 */

public class AddLocalArray extends Transformer {

    LocalArray la;

    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {

        // create a new local arraylist
        la = dc.createLocalArray(m, Integer.class);

    }

    @Override
    public void onMethodExit(Method m, MethodDynamicContext dc) {

        // gets the value of a local variable indexed 1 and adds to the array
        DynamicValue dv = dc.getLocalVariable(m, 1);
        dc.addToLocalArray(la, dv);

        // print the array values
        println("Values in list");
        print(la);

        // clear the array and print its values!
        dc.clearLocalArray(m, la);
        println("Values after clearing the list ");
        print(la);

    }
}
