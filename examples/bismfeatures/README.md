
### BISM Features

This folder has examples that highlight BISM features.

- Local Arrays: shows how to create a synthetic local array and populate it in a method and extract its values
- Local Variables: shows how to create a synthetic local variable.
- Static Fields: shows how to get and set a static field from the context of a traget program


## ```How to run```
 

You need [ant](https://ant.apache.org) installed on your machine.

Navigate to each example and from the terminal.

To run:

``` 
ant run
```

To modify and build an example:
``` 
ant build
```

To clean:
``` 
ant clean
```


You can change BISM args by changing their values in the build.xml in each folder.

To change the location of the BISM jar, change the **binaries** property in build.xml
