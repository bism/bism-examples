import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.LocalVariable;
import inria.bism.transformers.dynamiccontext.MethodDynamicContext;
import inria.bism.transformers.staticcontext.Method;

/***
 * This transformer demonstrates how synthesize a new local variable populate it with static data
 * and print it.
 */
public class AddLocalVariable extends Transformer {

    LocalVariable lv;


    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {
        lv = dc.addLocalVariable(m, 42);
    }

    @Override
    public void onMethodExit(Method m, MethodDynamicContext dc) {
        println("The value of synthetic local variable in " + m.name + "is : ");
        print(lv);
    }
}
