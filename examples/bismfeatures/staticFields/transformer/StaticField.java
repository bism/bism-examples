import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.DynamicValue;
import inria.bism.transformers.dynamiccontext.MethodDynamicContext;
import inria.bism.transformers.staticcontext.Method;

/*
 * This transformers retrieves a static field and update its value
 */

public class StaticField extends Transformer {

    @Override
    public void onMethodExit(Method m, MethodDynamicContext dc) {

        if (m.name.equals("main")) {

            DynamicValue message = dc.getStaticField(m, "message");
            println("Original message is: ");
            print(message);
            dc.setStaticField(m, "message", "Hello from transformer!"); // if desc is an int / long
                                                                        // field

            println("After Instrumentation message is: ");
            print(dc.getStaticField(m, "message"));
        }
    }

}
