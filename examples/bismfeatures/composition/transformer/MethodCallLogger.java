import inria.bism.transformers.Hidden;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.InstructionDynamicContext;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.dynamiccontext.MethodDynamicContext;
import inria.bism.transformers.staticcontext.BasicBlock;
import inria.bism.transformers.staticcontext.ClassContext;
import inria.bism.transformers.staticcontext.Method;
import inria.bism.transformers.staticcontext.MethodCall;

/***
 * This transformer demonstrates a simple transformer that prints a message at the enter/exit of a
 * method and a method call.
 * 
 */


public class MethodCallLogger extends Transformer {

    @Override
    public void onBasicBlockEnter(BasicBlock bb, InstructionDynamicContext dc) {
        println("Entered bb:" + bb.id);

    }

    @Override
    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

        println("Before calling method: " + mc.methodName);

    }

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {
        println("After calling method: " + mc.methodName);
    }

}
