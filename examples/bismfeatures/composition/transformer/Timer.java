import inria.bism.transformers.Hidden;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.DynamicValue;
import inria.bism.transformers.dynamiccontext.MethodDynamicContext;
import inria.bism.transformers.staticcontext.ClassContext;
import inria.bism.transformers.staticcontext.Method;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.MethodInsnNode;

/**
 * Instruments a timer into a target class that calculates the time taken by methods in milliseconds
 */
@Hidden
public class Timer extends Transformer {

    // public class C {
    // public static long timer;
    // public void m() throws Exception {
    // timer -= System.currentTimeMillis();
    // ..
    // ..
    // timer += System.currentTimeMillis();
    // }
    // }
    //

    @Override
    public void onClassEnter(ClassContext c) {
        c.addField(
                new FieldNode(Opcodes.ACC_PUBLIC | Opcodes.ACC_STATIC, "timer", "J", null, null));
    }

    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {

        insert(new FieldInsnNode(Opcodes.GETSTATIC, "Test", "timer", "J"));
        insert(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/System", "currentTimeMillis",
                "()J", false));
        insert(new InsnNode(Opcodes.LSUB));
        insert(new FieldInsnNode(Opcodes.PUTSTATIC, "Test", "timer", "J"));

    }

    @Override
    public void onMethodExit(Method m, MethodDynamicContext dc) {

        insert(new FieldInsnNode(Opcodes.GETSTATIC, "Test", "timer", "J"));
        insert(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/System", "currentTimeMillis",
                "()J", false));
        insert(new InsnNode(Opcodes.LADD));
        insert(new FieldInsnNode(Opcodes.PUTSTATIC, "Test", "timer", "J"));

        DynamicValue dv = dc.getStaticField(m, "timer");
        print("Time taken by method " + m.name + " (ms): ");
        print(dv);

    }
}
