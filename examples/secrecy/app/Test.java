import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class Test  {
    @Retention(RetentionPolicy.CLASS)
    @Target(ElementType.METHOD)
    @interface Secure {}


    @Retention(RetentionPolicy.CLASS)
    @Target(ElementType.METHOD)
    @interface SecretVariables {
        String[] secrets();
    }

    @SecretVariables(secrets = {"x"})
    public static void main(String[] args) {
        int x = 5;

        unsecureMethod(x, 5);
        method(x, 5);

        int y = x;

        System.out.println("y has to be x : "+y+ "(in fact x is 5)");

        int z = x + 42;

        unsecureMethod(z, 5+42);
        method(z, 5+42);

        System.out.println("Out");
    }


    private static void unsecureMethod(int a, int back){
        System.out.println("This is a revealed secret : "+a);
        System.out.println("Its real value (backuped) is : "+back);
    }

    @Secure
    private static void method(int a, int back){
        //Do some stuff to secure the revealing of a
        //...
        System.out.println("We securely reveal a secret : "+a);
        System.out.println("Its real value (backuped) is : "+back);
    }

}
