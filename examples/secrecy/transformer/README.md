@SuppressWarnings("unused")
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.METHOD)
@interface Secure {}

@SuppressWarnings("unused")
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.METHOD)
@interface SecretVariables {
    String[] secrets ();
}
  A transformer designed to enforce the property of non leaking a secret value.
 
  the secrets value are compile time decided with Annotation @SecretVariables({"var1",...}) (the annotation has to be
  defined in the source program via the preceding annotations
  the functions which are secured (do not leaks secret value) are annotated @Secure in the source program
 
  Then, we enforce the property that if V is a secret value, any direct leakage of V is forbidden via the two following
  rules :
  NonSecret <- V : Forbidden
  NonSecureOperation(V,...) : Forbidden
        This has effect only if you do not want to deload values everywhere, but rather want to see the
        information flow.
If the instruction is a secure transformation, do not do anything
For each value used by this instruction
For each instruction which creates this value
If this instruction loads a secret variable
Deload it
Potentially add new value to secrets
      Is the function declared secure in bytecode via @Secure ?
      Treatment of loading a secret value : pop it and loads a sample value.
      Whitelist of secure instructions and methods
Instructions which just push value onto stack
Instructions which put values into secrets
A compile time fixed list of method which are considered secure
Everything else cannot access secret data

