import inria.bism.transformers.Transformer;

import inria.bism.transformers.dynamiccontext.InstructionDynamicContext;
import inria.bism.transformers.dynamiccontext.MethodDynamicContext;
import inria.bism.transformers.staticcontext.ClassContext;
import inria.bism.transformers.staticcontext.Instruction;
import inria.bism.transformers.staticcontext.Method;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;
import org.objectweb.asm.*;
import org.objectweb.asm.tree.analysis.SourceValue;

import java.util.*;

/*
@SuppressWarnings("unused")
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.METHOD)
@interface Secure {}

@SuppressWarnings("unused")
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.METHOD)
@interface SecretVariables {
    String[] secrets ();
}
*/

/**
 * A transformer designed to enforce the property of non leaking a secret value.
 *
 * the secrets value are compile time decided with Annotation @SecretVariables({"var1",...}) (the annotation has to be
 * defined in the source program via the preceding annotations
 * the functions which are secured (do not leaks secret value) are annotated @Secure in the source program
 *
 * Then, we enforce the property that if V is a secret value, any direct leakage of V is forbidden via the two following
 * rules :
 * NonSecret <- V : Forbidden
 * NonSecureOperation(V,...) : Forbidden
 */
@SuppressWarnings("unused")
public class EnforceSecrecy extends Transformer {

    final int K = 0xBEEF;
    final Set<String> secureFunctions = new TreeSet<>();
    final Set<Integer> secretIndexes = new TreeSet<>();

    /* If a variable must take a secret value from a secret variable, makes it secret too
        This has effect only if you do not want to deload values everywhere, but rather want to see the
        information flow.
     */
    final boolean flowSecrecy = true;

    @Override
    public void onClassEnter(ClassContext c) {
        for (MethodNode m : c.classNode.methods){
            if (isAnnotatedSecure(m)){
                secureFunctions.add(m.name);
            }
        }
    }



    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {
        secretIndexes.clear();
        Set<String> secretVars = new TreeSet<>(getAnnotatedSecretVars(m));

        for (LocalVariableNode v : m.methodNode.localVariables) {
            if (secretVars.contains(v.name)) {
                secretIndexes.add(v.index);
            }
        }
    }

    @Override
    public void beforeInstruction(Instruction ins, InstructionDynamicContext dc) {
        //If the instruction is a secure transformation, do not do anything
        if (isSecure(ins))
            return;

        //For each value used by this instruction
        for (int i = 0; i < ins.getSourceValueFrame().getStackSize(); i += ins.getSourceValueFrame().getStack(i).size) {
            SourceValue sourceValue = ins.getSourceValueFrame().getStack(i);

            //For each instruction which creates this value
            for (AbstractInsnNode srcIns : sourceValue.insns) {

                //If this instruction loads a secret variable
                if (srcIns instanceof VarInsnNode && secretIndexes.contains(((VarInsnNode) srcIns).var))
                {
                    //Deload it
                    treatVarInsnNode(ins,srcIns);

                    //Potentially add new value to secrets
                    if (flowSecrecy && ins.node instanceof VarInsnNode) {
                        this.secretIndexes.add(((VarInsnNode)ins.node).var);
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    private Collection<String> getAnnotatedSecretVars(Method m) {
        ArrayList<String> secrets = new ArrayList<>();
        if (m.methodNode.invisibleAnnotations != null) {
            for (AnnotationNode an : m.methodNode.invisibleAnnotations) {
                if (an.desc.equals("LSecretVariables;")) {
                    secrets = (ArrayList<String>) an.values.get(1);
                    break;
                }
            }
        }
        return secrets;
    }
    /**
     * Is the function declared secure in bytecode via @Secure ?
     */
    private boolean isAnnotatedSecure(MethodNode m) {
        if (m.invisibleAnnotations == null)
            return false;
        for (AnnotationNode an : m.invisibleAnnotations) {
            if (an.desc.equals("LSecure;"))
                return true;
        }
        return false;
    }

    /**
     * Treatment of loading a secret value : pop it and loads a sample value.
     */
    private void treatVarInsnNode(Instruction ins, AbstractInsnNode srcIns) {
        List<AbstractInsnNode> newInstructions = new ArrayList<>();

        switch (srcIns.getOpcode()) {
            case Opcodes.LLOAD:
                newInstructions.add(new InsnNode(Opcodes.POP2));
                newInstructions.add(new LdcInsnNode((long) K));
                break;
            case Opcodes.DLOAD:
                newInstructions.add(new InsnNode(Opcodes.POP2));
                newInstructions.add(new LdcInsnNode((double) K));
            default:
                newInstructions.add(new InsnNode(Opcodes.POP));
                newInstructions.add(new LdcInsnNode(K));
                break;
        }
        ins.basicBlock.insertNextToInstruction(srcIns, newInstructions, false);
    }

    /**
     * Whitelist of secure instructions and methods
     */
    private boolean isSecure(Instruction ins) {

        //Instructions which just push value onto stack
        if (ins.opcode <= Opcodes.SALOAD){
            return true;
        }

        //Instructions which put values into secrets
        if (ins.node instanceof VarInsnNode && secretIndexes.contains((((VarInsnNode) ins.node).var)))
            return true;

        //A compile time fixed list of method which are considered secure
        if(ins.node instanceof MethodInsnNode)
            return secureFunctions.contains(((MethodInsnNode)ins.node).name);
        

        //Everything else cannot access secret data
        return false;
    }
}
