# BISM Examples

These examples demonstrate different Transformers that highlight BISM features and different use cases.

- BISM Features
- Java Good Practices
- Logging
- Fault Injection 
- Obfuscation
- Residual RV

The examples run BISM as an agent attached to the target app.

## How to run


 You need [ant](https://ant.apache.org) installed on your machine.

Navigate to each example and from the terminal.

To run interactively:

> If you want to have a colored output you must install the package [highlight](http://www.andre-simon.de/doku/highlight/en/highlight.php) in a repository which is in your PATH.

``` 
ant run
```

To modify and build an example:
``` 
ant build
```

To clean:
``` 
ant clean
```


You can change BISM args by changing their values in the bism.xml in each folder.
 
## Play with your own program

If you want to test BISM capabilities and write your own transformer, you can copy the `to-be-filled` folder and fill it according to your needs.

It is composed of a single transformer to experiment with, you can put your own jar or java files in the `app` folder and then run it to see what is done.
