import inria.bism.transformers.ASMFactory;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.dynamiccontext.MethodDynamicContext;
import inria.bism.transformers.staticcontext.ClassContext;
import inria.bism.transformers.staticcontext.Method;
import inria.bism.transformers.staticcontext.MethodCall;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.*;

import java.util.*;

/**
 * A Method Merging transformer which computes sets of compatibles methods (based on signature) and
 * then merge them into one method with the same signature and a new argument to decide which one to
 * execute.
 *
 * More explicitly : - All compatibles methods bytecode are copied into one corresponding Merger
 * method - If the compatibles have signature (T1,T2, ...)R, the corresponding Merger has signature
 * (T1,T2, ..., I)R - Calls to a compatible method M1 are replaced by calls to corresponding Merger
 * M' with : M'(M1 arguments, id(M1)) - All compatibles methods are deleted from the class file.
 *
 * We only merge instance methods (not statics) for convenience and to not have a too long code.
 */
public class Merging extends Transformer {

    // Desc->List<Method name>
    HashMap<String, List<String>> compatibles;
    // Desc->Merger
    HashMap<String, MethodNode> mergers;
    // Merger name -> Lookup Switch
    HashMap<String, LookupSwitchInsnNode> lookup;

    // Used to have a unique id for each modified method

    List<String> methodsId;

    @Override
    public void onClassEnter(ClassContext c) {
        compatibles = new HashMap<>();
        mergers = new HashMap<>();
        lookup = new HashMap<>();
        methodsId = new ArrayList<>();
        // Step 1 : Compute lists of compatible methods
        computeCompatibility(c);
        // Step 2 : Create a Merger method to each list
        createMergers(c);

    }

    private void computeCompatibility(ClassContext c) {
        for (MethodNode m : c.classNode.methods) {
            // Do not merge standards and static methods
            if (m.name.contains("init>") || m.name.equals("main")
                    || ((m.access & Opcodes.ACC_STATIC) != 0))
                continue;
            compatibles.putIfAbsent(m.desc, new ArrayList<>());
            compatibles.get(m.desc).add(m.name);
            c.markHasRemoved(m);
            methodsId.add(m.name + m.desc);
        }
    }

    private void createMergers(ClassContext c) {
        int i = 0;
        for (String desc : compatibles.keySet()) {

            // Compute descriptor and add merger to class
            List<Type> argTypes = new ArrayList<>(Arrays.asList(Type.getArgumentTypes(desc)));
            argTypes.add(Type.INT_TYPE);

            String mergerDesc = Type.getMethodDescriptor(Type.getReturnType(desc),
                    argTypes.toArray(new Type[0]));
            MethodNode mergerI =
                    new MethodNode(Opcodes.ACC_PUBLIC, "M" + (i++), mergerDesc, null, null);
            c.classNode.methods.add(mergerI);

            createMerger(mergerI, desc, argTypes);
        }
    }

    private void createMerger(MethodNode mergerI, String realDesc, List<Type> arguments) {
        // Compute basic incomplete code and add it to merger
        LabelNode end = new LabelNode();
        LookupSwitchInsnNode lookSwitch =
                new LookupSwitchInsnNode(end, new int[0], new LabelNode[0]);

        mergerI.instructions.add(ASMFactory.loadVar(Type.INT_TYPE, arguments.size()));
        mergerI.instructions.add(lookSwitch);
        // Here will go the methods code
        mergerI.instructions.add(end);
        // In order to be verifiable, add dead return
        switch (Type.getReturnType(mergerI.desc).getSize()) {
            case 1:
                mergerI.instructions.add(new InsnNode(Opcodes.ICONST_0));
                mergerI.instructions.add(new InsnNode(Opcodes.IRETURN));
                break;
            case 2:
                mergerI.instructions.add(new InsnNode(Opcodes.LCONST_0));
                mergerI.instructions.add(new InsnNode(Opcodes.LRETURN));
                break;
            default:
                mergerI.instructions.add(new InsnNode(Opcodes.RETURN));
        }
        // In order to retrieve it, store desc->Merger->LookupSwitch
        mergers.put(realDesc, mergerI);
        lookup.put(mergerI.name, lookSwitch);
    }

    @Override
    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {
        // If we encounter a call to a replaced method
        if (compatibles.getOrDefault(mc.methodNode.desc, new ArrayList<>()).contains(mc.methodName)
                && mc.methodOwner.equals(mc.currentClassName)) {

            LdcInsnNode loadMethodId =
                    new LdcInsnNode(methodsId.indexOf(mc.methodName + mc.methodNode.desc));

            // We must modify the method with ASM to keep tracks of what we did in MethodNode
            if (mc.ins.basicBlock.method.classContext.willBeRemoved(mc.ins.basicBlock.method)) {
                mc.ins.basicBlock.method.methodNode.instructions.insertBefore(mc.ins.node,
                        loadMethodId);
                mc.methodNode.name = mergers.get(mc.methodNode.desc).name;
                mc.methodNode.desc = mergers.get(mc.methodNode.desc).desc;
            }
            // Else modify with BISM and get it at end of methods
            else {
                insert(loadMethodId);
                remove(mc.ins);
                insert(new MethodInsnNode(mc.ins.opcode, mc.methodOwner,
                        mergers.get(mc.methodNode.desc).name,
                        mergers.get(mc.methodNode.desc).desc));
            }
        }

    }

    @Override
    public void onMethodExit(Method m, MethodDynamicContext dc) {
        if (m.classContext.willBeRemoved(m)) {
            LookupSwitchInsnNode lk = lookup.get(mergers.get(m.methodNode.desc).name);
            LabelNode beginMethod = (LabelNode) m.methodNode.instructions.getFirst();
            lk.keys.add(methodsId.indexOf(m.name + m.methodNode.desc));
            lk.labels.add(beginMethod);

            mergers.get(m.methodNode.desc).instructions.insert(lk, m.methodNode.instructions);
        }
    }
}
