  A Method Merging transformer which computes sets of compatibles methods (based on signature) and
  then merge them into one method with the same signature and a new argument to decide which one to
  execute.
 
  More explicitly : - All compatibles methods bytecode are copied into one corresponding Merger
  method - If the compatibles have signature (T1,T2, ...)R, the corresponding Merger has signature
  (T1,T2, ..., I)R - Calls to a compatible method M1 are replaced by calls to corresponding Merger
  M' with : M'(M1 arguments, id(M1)) - All compatibles methods are deleted from the class file.
 
  We only merge instance methods (not statics) for convenience and to not have a too long code.
 Desc->List<Method name>
 Desc->Merger
 Merger name -> Lookup Switch
 Used to have a unique id for each modified method
 Step 1 : Compute lists of compatible methods
 Step 2 : Create a Merger method to each list
 Do not merge standards and static methods
 Compute descriptor and add merger to class
 Compute basic incomplete code and add it to merger
 Here will go the methods code
 In order to be verifiable, add dead return
 In order to retrieve it, store desc->Merger->LookupSwitch
 If we encounter a call to a replaced method
 We must modify the method with ASM to keep tracks of what we did in MethodNode
 Else modify with BISM and get it at end of methods

