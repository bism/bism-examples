import java.io.IOException;

public class Test {

    public static void main(String[] args) throws IOException {
        int a = 2;
        Test t = new Test();
        t.method1(args);
        t.m();
        t.g();
        h();
        a = t.a(a);

    }

    public void method1(String[] args) throws IOException {
        String name = "method1";
        m();
    }

    public void method2(String[] args) throws IOException {
        String name = "method2";
        method1(args);
    }

    public void method3(String[] args) throws IOException {
        String name = "method3";
        switch (args.length){
            case 0:
                method1(args);
                break;
            case 1:
                method2(args);
            default:
                break;
        }
    }

    public void m() throws IOException {
        String name = "m";
        g();
    }

    public void g() throws IOException {
        String name = "g";
        Test.h();
    }

    public void MG() throws IOException {
        String name = "MG";
        if (true)
            m();
        else if (false)
            g();
        else
            MG();
    }

    public static void h() throws IOException {
        String name = "h";
    }

    public int a(int b) throws IOException {
        String name = "a";
        method1(new String[0]);
        return 1;
    }

    public int b(int b) throws IOException {
        String name = "b";
        method1(new String[0]);
        return 1;
    }

    public int c(int b) throws IOException {
        String name = "c";
        method1(new String[0]);
        return 1;
    }

    public long d() {
        String name = "d";
        return 1;
    }

    public long e(){
        String name = "e";
        while (true){
            switch (1){
                case 0:
                    continue;
                case 82:
                    return 2;
                case 2:
                    return 3;
                default:
                    ;
            }
            if (true){
                break;
            }
        }
        return 5;
    }
}
