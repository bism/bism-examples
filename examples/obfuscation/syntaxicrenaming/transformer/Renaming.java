import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.InstructionDynamicContext;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.dynamiccontext.MethodDynamicContext;
import inria.bism.transformers.staticcontext.ClassContext;
import inria.bism.transformers.staticcontext.Instruction;
import inria.bism.transformers.staticcontext.Method;
import inria.bism.transformers.staticcontext.MethodCall;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.TreeSet;

/**
 * A simple syntaxic obfuscator which works on bytecode all variables / methods / fields are
 * replaced by their hashcode Here we need to access following joinpoints : - Class Entering : To
 * rename fields and methods - Method Entering : To rename local variables and change their "debug"
 * types - Before Method Call : To rename the call to obfuscated methods - Before Instruction : To
 * rename access to obfuscated fields
 *
 * This transformer enforces the unreadability of the decompiled source code, while preserving the
 * semantic
 */
public class Renaming extends Transformer {

    // Here you must specify which classes will be obfuscated, this is necessary as we do not have
    // access to the scope
    private final Collection<String> obfuscatedClasses =
            new TreeSet<>(Arrays.asList("Test", "SubClass"));

    // A prefix to the renamed objects, use of UTF-8 is valid in bytecode, not in source code.
    final String prefix = "¬";

    private String rename(String owner, String name) {
        // Do not rename standard methods
        if (name.contains("<init>") || name.contains("<clinit>") || name.equals("main"))
            return name;
        else if (obfuscatedClasses.contains(owner))
            return prefix + Math.abs(name.hashCode());
        else
            return name;
    }

    @Override
    public void onClassEnter(ClassContext c) {
        for (FieldNode f : c.classNode.fields) {
            f.name = rename(c.name, f.name);
        }
        for (MethodNode m : c.classNode.methods) {
            m.name = rename(c.name, m.name);
        }
    }

    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {
        for (LocalVariableNode v : m.methodNode.localVariables) {
            v.name = rename(m.className, v.name);
            v.desc = Type.BYTE_TYPE.getDescriptor();
        }
    }

    @Override
    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {
        remove(mc.ins);
        insert(new MethodInsnNode(mc.ins.opcode, mc.methodOwner,
                rename(mc.methodOwner, mc.methodName), mc.methodNode.desc));
    }

    @Override
    public void beforeInstruction(Instruction ins, InstructionDynamicContext dc) {
        if (ins.node instanceof FieldInsnNode) {
            remove(ins);
            insert(new FieldInsnNode(ins.opcode, ((FieldInsnNode) ins.node).owner,
                    rename(((FieldInsnNode) ins.node).owner, ((FieldInsnNode) ins.node).name),
                    ((FieldInsnNode) ins.node).desc));
        }
    }
}
