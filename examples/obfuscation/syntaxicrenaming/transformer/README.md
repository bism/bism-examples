  A simple syntaxic obfuscator which works on bytecode all variables / methods / fields are
  replaced by their hashcode Here we need to access following joinpoints : - Class Entering : To
  rename fields and methods - Method Entering : To rename local variables and change their "debug"
  types - Before Method Call : To rename the call to obfuscated methods - Before Instruction : To
  rename access to obfuscated fields
 
  This transformer enforces the unreadability of the decompiled source code, while preserving the
  semantic
 Here you must specify which classes will be obfuscated, this is necessary as we do not have
 access to the scope
 A prefix to the renamed objects, use of UTF-8 is valid in bytecode, not in source code.
 Do not rename standard methods

