
import inria.bism.cfg.BlockType;
import inria.bism.transformers.ASMFactory;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.InstructionDynamicContext;
import inria.bism.transformers.dynamiccontext.LocalVariable;
import inria.bism.transformers.dynamiccontext.MethodDynamicContext;
import inria.bism.transformers.staticcontext.BasicBlock;
import inria.bism.transformers.staticcontext.Method;
import org.objectweb.asm.Type;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;



import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A Control Flow Flattening transformer, with this transformer methods CFG is flattened into a new
 * CFG which gives no information and is determined at runtime.
 *
 * More explicitly : - All Basic blocks remains - A new "Dispatcher" basic block is added, with all
 * basic blocks as successors - A jump from BB1 to BB2 is replaced by the following sequence : - BB1
 * affects control variable to BB2 - BB1 jumps to the dispatcher - The dispatcher switches on
 * control variable to find where to jump (here BB2)
 *
 * We get a three level flattening because of conditional jumps which are handled as : - set the
 * control variables into new basic blocks (one for each branch) - goto dispatcher Warning : Must
 * not be applied to constructors, since it adds new variables and potentially overload this
 */
public class CFFlattening extends Transformer {
    LocalVariable ctrl;
    LabelNode dispatcherLabel;
    LabelNode interm_f;



    LabelNode interm_t;

    private int opacify(int id) {
        return id;
    }

    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {
        // In order to not have frame problems, we must ensure local variables are affected since
        // beginning.
        // But we must not touch arguments, so ignore them
        for (LocalVariableNode l : m.methodNode.localVariables) {
            if (l.index < Type.getArgumentTypes(m.methodNode.desc).length)
                continue;
            Type type = Type.getType(l.desc);
            if (type.getSort() == Type.OBJECT) {
                insert(new InsnNode(Opcodes.ACONST_NULL));
            } else if (type.getSize() == 1) {
                insert(new LdcInsnNode(0));
            } else {
                insert(new LdcInsnNode((long) 0));
            }
            insert(ASMFactory.storeVar(type, l.index));
        }

        // Add a new local variable "control"
        // control <- opacify(m.getEntryBlock().id)
        dispatcherLabel = new LabelNode();
        ctrl = dc.addLocalVariable(m, opacify(m.getEntryBlock().id));
        insert(dispatcherLabel);
        LabelNode[] flattening = new LabelNode[m.getNumberOfBasicBlocks()];
        LabelNode entryBlock = new LabelNode();

        Set<BasicBlock> visited = new HashSet<>();
        List<BasicBlock> toVisit = new ArrayList<>();
        toVisit.add(m.getEntryBlock());
        int min = m.getNumberOfBasicBlocks();
        int max = m.getEntryBlock().id;

        for (int i = 0; i < toVisit.size(); i++) {
            BasicBlock bb = toVisit.get(i);
            if (visited.contains(bb))
                continue;
            if (bb.id < min) {
                min = bb.id;
            }
            if (bb.id > max) {
                max = bb.id;
            }
            if (m.getEntryBlock() == bb) {
                flattening[bb.id - m.getEntryBlock().id] = entryBlock;
            } else {
                flattening[bb.id - m.getEntryBlock().id] = (LabelNode) bb.getFirstInstruction();
            }
            visited.add(bb);
            toVisit.addAll(bb.getSuccessorBlocks());
        }
        /*
         * dispatcher_label: /* switch(control){ case opacify(BB1.id): goto BB1.label ...
         */
        insert(ASMFactory.loadVar(Type.INT_TYPE, ctrl.index));
        insert(new TableSwitchInsnNode(min, max,
                (LabelNode) m.getEntryBlock().getFirstInstruction(), flattening));
        insert(entryBlock);

    }

    @Override
    public void onBasicBlockEnter(BasicBlock bb, InstructionDynamicContext dc) {
        if (bb.blockType == BlockType.CONDJUMP) {
            interm_t = new LabelNode();
            interm_f = new LabelNode();
        }
    }

    @Override
    public void onBasicBlockExit(BasicBlock bb, InstructionDynamicContext dc) {
        // If goto block
        // Add "control" opacify successor.id
        // Replace goto(blabla) -> goto dispatcher
        if (bb.blockType == BlockType.GOTO) {
            insert(new LdcInsnNode(bb.getSuccessorBlocks().get(0).id));
            insert(ASMFactory.storeVar(Type.INT_TYPE, ctrl.index));
            remove(bb.getLast());
            insert(new JumpInsnNode(Opcodes.GOTO, dispatcherLabel));
        }
        // If condjump
        // Create two label nodes interm_t ; interm_f
        // Replace IFX label_t -> IFX interm_t
        // add goto interm_f
        else if (bb.blockType == BlockType.CONDJUMP) {
            remove(bb.getLast());
            insert(new JumpInsnNode(bb.getLast().opcode, interm_t));
            insert(new JumpInsnNode(Opcodes.GOTO, interm_f));
        }
        // If switch
        // For i = 0; i < labels.size ; i++
        // l' <- new label
        //
        // insert(l', c <- bbByLabel(labels[i]).id, goto dispatch)
        // labels.set(i,l')
        else if (bb.blockType == BlockType.SWITCH) {
            AbstractInsnNode sw = bb.getFirstInstruction();
            List<LabelNode> labels = null;
            if (sw.getType() == AbstractInsnNode.TABLESWITCH_INSN) {
                labels = ((TableSwitchInsnNode) sw).labels;
            } else if (sw.getType() == AbstractInsnNode.LOOKUPSWITCH_INSN) {
                labels = ((LookupSwitchInsnNode) sw).labels;
            }

            for (int i = 0; i < labels.size(); i++) {
                LabelNode l_new = new LabelNode();
                insert(l_new);
                insert(new LdcInsnNode(bbByLabel(labels.get(i), bb).id));
                insert(ASMFactory.storeVar(Type.INT_TYPE, ctrl.index));
                insert(new JumpInsnNode(Opcodes.GOTO, dispatcherLabel));
                labels.set(i, l_new);
            }
        } else if (bb.blockType != BlockType.RETURN && bb.getSuccessorBlocks().size() > 0) {
            insert(new LdcInsnNode(bb.getSuccessorBlocks().get(0).id));
            insert(ASMFactory.storeVar(Type.INT_TYPE, ctrl.index));
            insert(new JumpInsnNode(Opcodes.GOTO, dispatcherLabel));
        }
    }

    @Override
    public void onTrueBranchEnter(BasicBlock jumpingBlock, InstructionDynamicContext dc) {
        ifFlattening(jumpingBlock.getTrueBranch(), interm_t);
    }

    @Override
    public void onFalseBranchEnter(BasicBlock jumpingBlock, InstructionDynamicContext dc) {
        ifFlattening(jumpingBlock.getFalseBranch(), interm_f);
    }

    private BasicBlock bbByLabel(LabelNode labelNode, BasicBlock bb) {
        for (BasicBlock succ : bb.getSuccessorBlocks()) {
            if (succ.getFirstInstruction().equals(labelNode))
                return succ;
        }
        return null;
    }

    private void ifFlattening(BasicBlock where, LabelNode interm) {
        // Add interm:
        // Add control = opacify bb.id
        // Add goto dispatcher
        List<AbstractInsnNode> toAdd = new ArrayList<>();
        toAdd.add(interm);
        toAdd.add(new LdcInsnNode(where.id));
        toAdd.add(ASMFactory.storeVar(Type.INT_TYPE, ctrl.index));
        toAdd.add(new JumpInsnNode(Opcodes.GOTO, dispatcherLabel));
        where.insertNextToInstruction(where.getFirstInstruction(), toAdd, true);

    }
}
