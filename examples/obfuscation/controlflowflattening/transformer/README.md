  A Control Flow Flattening transformer, with this transformer methods CFG is flattened into a new
  CFG which gives no information and is determined at runtime.
 
  More explicitly : - All Basic blocks remains - A new "Dispatcher" basic block is added, with all
  basic blocks as successors - A jump from BB1 to BB2 is replaced by the following sequence : - BB1
  affects control variable to BB2 - BB1 jumps to the dispatcher - The dispatcher switches on
  control variable to find where to jump (here BB2)
 
  We get a three level flattening because of conditional jumps which are handled as : - set the
  control variables into new basic blocks (one for each branch) - goto dispatcher Warning : Must
  not be applied to constructors, since it adds new variables and potentially overload this
 In order to not have frame problems, we must ensure local variables are affected since
 beginning.
 But we must not touch arguments, so ignore them
 Add a new local variable "control"
 control <- opacify(m.getEntryBlock().id)
          dispatcher_label: / switch(control){ case opacify(BB1.id): goto BB1.label ...
 If goto block
 Add "control" opacify successor.id
 Replace goto(blabla) -> goto dispatcher
 If condjump
 Create two label nodes interm_t ; interm_f
 Replace IFX label_t -> IFX interm_t
 add goto interm_f
 If switch
 For i = 0; i < labels.size ; i++
 l' <- new label

 insert(l', c <- bbByLabel(labels[i]).id, goto dispatch)
 labels.set(i,l')
 Add interm:
 Add control = opacify bb.id
 Add goto dispatcher

