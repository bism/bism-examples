# Obfuscation

This folder contains examples on how to obfuscate binaries and enforce the *unreadability* of decompiled binaries.

## Types of obfuscation implemented

- Syntaxic renaming : Simple use case of syntaxical changes, all fields / methods / variables are renamed to uncomprehensive names.
- Junk code adding : Based on opaque predicate we add code which will be uncompilable but never runned.
- Method Merging : All methods which have the same signature are merged into one method which will choose between them, calls are replaced.
- Control Flow Flattening : Makes the control flow at runtime instead of statically.

## ```How to run```


You need [ant](https://ant.apache.org) installed on your machine.

Navigate to each example and from the terminal.

To run:

``` 
ant run
```

To modify and build an example:
``` 
ant build
```

To clean:
``` 
ant clean
```


You can change BISM args by changing their values in the build.xml in each folder.

To change the location of the BISM jar, change the **binaries** property in build.xml

## How to test disassembly

First you can try to view the raw bytecode of the obfuscated binaries with the command : `javap -v path/to/Example.class`.

If you want to try disassembling you can try tools listed on [http://www.javadecompilers.com/](http://www.javadecompilers.com/).
