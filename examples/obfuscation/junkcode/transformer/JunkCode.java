import inria.bism.transformers.ASMFactory;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.MethodDynamicContext;
import inria.bism.transformers.staticcontext.Method;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.TypeInsnNode;

/**
 * A simple proof of concept of making decompilation impossible
 * We transform methods into :
 *      if (opaquePredicate()){
 *          goto junk;
 *      }
 *      real:
 *      ... real method code ...
 *      goto end;
 *      junk:
 *      ... junky code ...
 *      goto real;
 *      end:
 *      ... method return ...
 * so that this code could not be compiled anymore.
 *
 * Problem : The class now needs to be run without verifier (-noverify), since there is garbage code inside.
 */
public class JunkCode extends Transformer {


    static LabelNode l_junk;
    static LabelNode l_real;
    static LabelNode l_end;
    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {

        l_real = new LabelNode();
        l_junk = new LabelNode();
        l_end = new LabelNode();

        insertOpaquePredicate(m, l_real, l_junk);
    }

    @Override
    public void onMethodExit(Method m, MethodDynamicContext dc) {
        insert(new JumpInsnNode(Opcodes.GOTO, l_end));

        //junky code
        insert(new InsnNode(Opcodes.POP));
        //To let the user think this code has an importance and needs to be executed before real code
        insert(new JumpInsnNode(Opcodes.GOTO, l_real));

        insert(l_end);
    }

    private void insertOpaquePredicate(Method m, LabelNode real_code, LabelNode junk_code){

        /* Calculation */
        insert(new TypeInsnNode(Opcodes.NEW, "java/util/Random"));
        insert(new InsnNode(Opcodes.DUP));
        insert(ASMFactory.invokeSpecial("java/util/Random", "<init>", "()V"));
        insert(new InsnNode(Opcodes.DUP));
        insert(ASMFactory.invokeVirtual("java/util/Random", "nextInt", "()I"));
        insert(ASMFactory.invokeStatic("java/lang/Math", "abs", "(I)I"));
        insert(new InsnNode(Opcodes.SWAP));
        insert(ASMFactory.invokeVirtual("java/util/Random", "nextInt", "()I"));
        insert(ASMFactory.invokeStatic("java/lang/Math", "abs", "(I)I"));
        insert(new InsnNode(Opcodes.DUP_X1));
        insert(new InsnNode(Opcodes.IOR));

        /* jumping */
        insert(new JumpInsnNode(Opcodes.IF_ICMPGT, junk_code));

        /* Real code beginning */
        insert(real_code);

        m.methodNode.maxStack += 3;
        m.methodNode.maxLocals += 1;
    }
}
