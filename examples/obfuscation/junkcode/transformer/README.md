  A simple proof of concept of making decompilation impossible
  We transform methods into :
       if (opaquePredicate()){
           goto junk;
       }
       real:
       ... real method code ...
       goto end;
       junk:
       ... junky code ...
       goto real;
       end:
       ... method return ...
  so that this code could not be compiled anymore.
 
  Problem : The class now needs to be run without verifier (-noverify), since there is garbage code inside.
junky code
To let the user think this code has an importance and needs to be executed before real code
        insert(new TypeInsnNode(Opcodes.NEW, "java/util/Random"));
        insert(new InsnNode(Opcodes.DUP));
        insert(ASMFactory.invokeSpecial("java/util/Random", "<init>", "()V"));
        insert(new InsnNode(Opcodes.DUP));
        insert(ASMFactory.invokeVirtual("java/util/Random", "nextInt", "()I"));
        insert(ASMFactory.invokeStatic("java/lang/Math", "abs", "(I)I"));
        insert(new InsnNode(Opcodes.SWAP));
        insert(ASMFactory.invokeVirtual("java/util/Random", "nextInt", "()I"));
        insert(ASMFactory.invokeStatic("java/lang/Math", "abs", "(I)I"));
        insert(new InsnNode(Opcodes.DUP_X1));
        insert(new InsnNode(Opcodes.IOR));

        insert(real_code);

        m.methodNode.maxStack += 3;
        m.methodNode.maxLocals += 1;
    }
}

