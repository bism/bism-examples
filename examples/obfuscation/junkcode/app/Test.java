class SubClass {
    long fieldLong;
    String fieldStr;

    public String identity(){
        return fieldStr+fieldLong;
    }
}

public class Test {
    int fieldInt;

    public static void main(String[] args)  {
        
        Test t = new Test();
        t.fieldInt = 34;
        
        SubClass c = new SubClass();
        c.fieldLong = 42;
        c.fieldStr = "Me ";
        
        System.out.println(c.identity());
        
        System.out.println(method(c.identity(), 0, 2));
        
        t.add(t.fieldInt, t.fieldInt+1);
        t.toDoubleLong(t.fieldInt);
        
    }

    public static String method(String str0, int arg1, long arg2){
        String local1 = str0.substring(arg1, (int) arg2);
        return local1.toUpperCase();
    }

    private int add(int first, int second){
        int temp = first + second;
        fieldInt++;
        return temp;
    }

    private long toDoubleLong(int a){
        return add(a,a);
    }
}
