import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.DynamicValue;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;

import inria.bism.transformers.staticcontext.MethodCall;

public class HasNextTransformer extends Transformer {

    @Override
    public void beforeMethodCall(MethodCall methodCall, MethodCallDynamicContext dc) {

        if (methodCall.methodName.contains("hasNext") && methodCall.methodOwner.contains("Iterator")) {

            DynamicValue it = dc.getMethodReceiver(methodCall);

            StaticInvocation sti =
                    new StaticInvocation("Monitor", "hasNextEvent");
            sti.addParameter(it);
            invoke(sti);

        }

    }

    @Override
    public void afterMethodCall(MethodCall methodCall, MethodCallDynamicContext dc) {

        if (methodCall.methodName.contains("next") && methodCall.methodOwner.contains("Iterator")) {
            DynamicValue it = dc.getMethodReceiver(methodCall);

            StaticInvocation sti =
                    new StaticInvocation("Monitor", "nextEvent");
            sti.addParameter(it);
            invoke(sti);
        }

    }

}
