This transformer demonstrates how we can **extract events** to monitor the `HasNext` property 
which specifies that a program should always call hasNext() before calling next() on an iterator.
The events are sent to a demonstration monitor named Monitor bundled with the target program.
