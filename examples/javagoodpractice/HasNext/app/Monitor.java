
public class Monitor {
 

    public static void hasNextEvent(Object i) {
         
        System.out.println("hasNext event on iterator with hash id: " + System.identityHashCode(i)); 
        
    }

    public static void nextEvent(Object i) {

         System.out.println("next event on iterator with hash id: " + System.identityHashCode(i)); 

    }

}
