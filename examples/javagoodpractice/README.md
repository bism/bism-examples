
### Java Good Practices

In this folder, we demonstrate BISM transformers that extract traces from a Java program to monitor the following properties:

- HasNext property 
    - specifies that a program should always call hasNext() before calling next() on an iterator. 
- UnSafeIterator property 
    - specifies that a collection should not be updated when an iterator asso- ciated with it is being used.
- SafeLock:
	- specifies that the number of acquires and releases of a Lock class are matched within a given method call.


## ```How to run```
 
You need [ant](https://ant.apache.org) installed on your machine.

Navigate to each example and from the terminal.

To run:

``` 
ant run
```

To modify and build an example:
``` 
ant build
```

To clean:
``` 
ant clean
```


You can change BISM args by changing their values in the build.xml in each folder.

To change the location of the BISM jar, change the **binaries** property in build.xml
