import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.DynamicValue;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;

import inria.bism.transformers.staticcontext.MethodCall;

/***
 * This transformer demonstrates how we can extract events to monitor the UnSafeIterator property 
 * which specifies that a collection should not be updated when an iterator associated with it is being used
 * The events are sent to a demonstration monitor named Monitor bundled with the target program.
 */

public class UnSafeIteratorTransformer extends Transformer {

    @Override
    public void beforeMethodCall(MethodCall methodCall, MethodCallDynamicContext dc) {

        if (methodCall.methodName.contains("add") && methodCall.methodOwner.contains("Collection")) {

            DynamicValue it = dc.getMethodReceiver(methodCall);

            //invoke monitor
            StaticInvocation sti =
                    new StaticInvocation("Monitor", "updatedCollection");
            sti.addParameter(it);
            invoke(sti);

        }

    }

    @Override
    public void afterMethodCall(MethodCall methodCall, MethodCallDynamicContext dc) {

        if (methodCall.methodName.contains("iterator") && methodCall.methodOwner.contains("Collection")) {

            DynamicValue it = dc.getMethodReceiver(methodCall);
            DynamicValue r = dc.getMethodResult(methodCall);

            //invoke monitor
            StaticInvocation sti =
                    new StaticInvocation("Monitor", "createdIterator");
            sti.addParameter(r);
            sti.addParameter(it);
            invoke(sti);

        }

    }

}
