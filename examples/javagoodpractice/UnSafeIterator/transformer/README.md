  This transformer demonstrates how we can extract events to monitor the UnSafeIterator property 
  which specifies that a collection should not be updated when an iterator associated with it is being used
  The events are sent to a demonstration monitor named Monitor bundled with the target program.
invoke monitor
invoke monitor

