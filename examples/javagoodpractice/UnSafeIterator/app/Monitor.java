
import java.util.*;

public class Monitor {
 
    public static void updatedCollection(Object i) {

    	System.out.println("Updated Collection (id) : " + System.identityHashCode(i)); 

    }

    public static void createdIterator(Iterator i, Object s) {

        System.out.println("Created Iterator (id) : " + System.identityHashCode(i) 
        	+ " from Collection (id):" + System.identityHashCode(s)); 
    }

}
