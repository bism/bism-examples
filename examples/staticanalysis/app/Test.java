import java.util.*;

public class Test {

    public static void main(String[] args) {

        List collection = new ArrayList<>();
        collection.add(1); // update event

        List collection2 = new ArrayList<>();

        Iterator it = collection.iterator(); // create event

        if (collection2.isEmpty()) {
            collection2.add(1); // update event
            collection2.add(2); // update event
        }

        doSomething(it.next()); // next event

        it = collection.iterator(); // create event
        it.next(); // next event
    }

    private static void doSomething(Object next) {}

}
