import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.DynamicValue;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.staticcontext.MethodCall;

import java.util.Arrays;
import java.util.List;

public class UnsafeIteratorDynamic extends Transformer {

    @Override
    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {


        List<String> updateMethods = Arrays.asList("add", "remove", "set", "removeAll", "sort",
                "retainAll", "addAll", "replaceAll", "clear");

        for (String um : updateMethods) {
            if (mc.methodName.contains(um) && mc.methodOwner.contains("List")) {

                // DynamicValue it = dc.getMethodReceiver(mc);


                // StaticInvocation sti =
                // new StaticInvocation("mycustommonitors/HasNextRuntimeMonitor", "updatedList");
                // sti.addParameter(it);
                // invoke(sti);

                println("u");

                break;

            }
        }
    }

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

        if (mc.methodName.contains("iterator") && mc.methodOwner.contains("List")) {

            // DynamicValue it = dc.getMethodReceiver(mc);
            // DynamicValue r = dc.getMethodResult(mc);

            // StaticInvocation sti =
            // new StaticInvocation("mycustommonitors/HasNextRuntimeMonitor", "createdIterator");
            // sti.addBoxedParameter(r);
            // sti.addParameter(it);


            // invoke(sti);

            println("c");
        }

        if (mc.methodName.contains("next") && mc.methodOwner.contains("Iterator")) {
            // DynamicValue it = dc.getMethodReceiver(mc);

            // StaticInvocation sti =
            // new StaticInvocation("mycustommonitors/HasNextRuntimeMonitor", "nextEvent");
            // sti.addParameter(it);
            // invoke(sti);

            println("n");
        }
    }
}
