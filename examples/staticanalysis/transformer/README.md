 DynamicValue it = dc.getMethodReceiver(mc);
 StaticInvocation sti =
 new StaticInvocation("mycustommonitors/HasNextRuntimeMonitor", "updatedList");
 sti.addParameter(it);
 invoke(sti);
 DynamicValue it = dc.getMethodReceiver(mc);
 DynamicValue r = dc.getMethodResult(mc);
 StaticInvocation sti =
 new StaticInvocation("mycustommonitors/HasNextRuntimeMonitor", "createdIterator");
 sti.addBoxedParameter(r);
 sti.addParameter(it);
 invoke(sti);
 DynamicValue it = dc.getMethodReceiver(mc);
 StaticInvocation sti =
 new StaticInvocation("mycustommonitors/HasNextRuntimeMonitor", "nextEvent");
 sti.addParameter(it);
 invoke(sti);

