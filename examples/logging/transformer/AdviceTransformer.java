import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.dynamiccontext.MethodDynamicContext;
import inria.bism.transformers.staticcontext.Method;
import inria.bism.transformers.staticcontext.MethodCall;

/***
 * This transformer demonstrates a simple transformer 
 * that prints a message at the enter/exit of a method and a method call.
 */
public class AdviceTransformer extends Transformer {

    @Override
    public void onMethodExit(Method m, MethodDynamicContext dc) {
        println("Exiting  method: " + m.name);
    }

    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {
        println("Entering  method: " + m.name);
    }

    @Override
    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {
      println("Before calling method: " + mc.methodName);
    }

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {
        println("After calling method: " + mc.methodName);
    }

}
