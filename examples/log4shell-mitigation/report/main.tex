\documentclass[a4paper]{article}

\usepackage{listings}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{xcolor}
\usepackage{url}
%
\definecolor{white}{rgb}{1,1,1}
\definecolor{backgroundColour}{rgb}{0.95,0.95,0.92}
\definecolor{mGray}{rgb}{0.5,0.5,0.5}
\lstdefinestyle{JavaStyle}{
	language=Java,
	tabsize=2,
	breaklines=true,
	breakatwhitespace=true,
	escapechar=|*,
	numbers=left,                    
	numbersep=5pt,
	stepnumber=1,
	aboveskip=\baselineskip,
	captionpos=b,
	frame=single,
	columns=fullflexible,
	showstringspaces=false,
	extendedchars=true,
	breaklines=true,
	showtabs=false,
	showspaces=false,
	identifierstyle=\ttfamily,
	morekeywords={String}%{byteArrayCompare},
	float=tp
}

\lstset{
	basicstyle=\fontsize{8.5pt}{10pt}\ttfamily,
	numberstyle=\footnotesize\ttfamily\color{mGray},
	keywordstyle=\color[rgb]{0.498,0.0,0.333},
	stringstyle=\color[rgb]{0.165,0.0,0.999},
	commentstyle=\color[rgb]{0.247,0.498,0.372},
	backgroundcolor=\color{backgroundColour},
}

%opening
\title{Log4Shell Mitigation, using BISM}
\author{Marius Monnier}
\date{Univ. Grenoble Alpes, Inria, CNRS, Grenoble INP, Laboratoire d'Informatique de Grenoble, 38000 Grenoble France}
\begin{document}

\maketitle

\begin{abstract}
This report present a simple mitigation for the \emph{Log4Shell} vulnerability.
%
\emph{Log4Shell} is present in many Java applications using the \emph{Log4j} library, distributed by Apache, and causes a remote code execution potentiality.
%
To mitigate that attack, we propose an automated code transformation which permits to remove the vulnerable code at runtime.
%
For this, we rely on BISM~\cite{bism}, our bytecode transformation tool.
%
Finally, we provide a user manual to protect any Java program.
\end{abstract}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
The \emph{Log4Shell} vulnerability~\cite{log4shell} has been discovered in December 2021 and permits to do Remote Code Execution in Java Applications.
%
According to actual recommendations, the simplest way to protect from \emph{Log4Shell} is to forbid the use of the class loading feature.
%
However, systematically disabling the feature is not satisfactory, as it may be necessary to the functioning of actual programs.
%
In this report, we demonstrate how to obtain two mitigation techniques that permits continuing using the application.

\paragraph{Report structure.}
%
We first recall the \emph{Log4Shell} vulnerability as described in~\cite{log4shell}.
%
Then, we briefly explain BISM, the tool we used to mitigate the vulnerability, a more formal and detailed presentation is done in a recent paper~\cite{bism}.
%
According to that, we present our mitigation and how we implement it in BISM language.
%
Finally, we describe how to use it in order to protect Java programs.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Preliminaries}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
First, we review some preliminary concepts before presenting our mitigations with BISM.
%
Namely, we review the Log4Shell vulnerability (Section~\ref{sec:prelim:log4s}) and the BISM tool (Section~\ref{sec:prelim:bism}).
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Log4Shell Vulnerability (\emph{CVE-2021-44228})}
\label{sec:prelim:log4s}
%
The \emph{Log4Shell} vulnerability~\cite{log4shell} has been discovered in December 2021 and permits to do Remote Code Execution in Java Applications.

It is based on the \emph{JNDI} feature, embedded into the \emph{Log4j} library which permits to load classes from an attacker controller endpoint.
%
The \emph{Log4j} is a logging library used as the standard one in many Java applications.
%
In order to use the vulnerability, the attacker has to access the log facility through a non-sanitized input such as the one presented in Listing~\ref{lst:vulnerable}.

\begin{figure}
	\begin{lstlisting}[style=JavaStyle,label=lst:vulnerable,caption=A sample class vulnerable to \emph{Log4Shell}]
	import org.apache.logging.log4j.LogManager;
	import org.apache.logging.log4j.Logger;
	
	public class A {
		Logger logger = LogManager.getLogger("My-Logger");
	
		public void function(String userControlledInput) {
			logger.info("User entered " + userControlledInput);
		}
	}
	\end{lstlisting}
\end{figure}

In this example, \lstinline[language=Java]|userControlledInput| is not sanitized and, therefore an attacker can set it to any meaningful value.
%
\emph{Log4j} proposes an advanced feature which is the variable substitution, with multiple usable pattern.
%
The one we are interested in is the \emph{JNDI} pattern, which permits to load a class through an \emph{LDAP} endpoint.
%
In short, the string \lstinline|"${jndi:ldap://host/A.class}| allows \emph{Log4j} to load class \lstinline|A| into memory.

This permits \emph{Remote-Code Execution} on any application which does not disable the JNDI lookups or other patterns which permits to load classes.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{BISM presentation and workflow}
\label{sec:prelim:bism}

We now briefly present BISM~\cite{bism}, a tool which permits to instrument Java programs at the bytecode level simply and efficiently.

BISM is a lightweight tool, programmed in Java and portable which has no dependency.
%
It is based on the ASM~\cite{asm} library, a bytecode parsing library offering a rich API to manipulate bytecode.

The BISM workflow permits to embed bytecode into classes, following an event-based model.
%
Its instrumentation loop is demonstrated in Figure~\ref{fig:bism-loop} and a simple view of its internal workflow in Figure~\ref{fig:bism-workflow}.

With the event-based model, one can subscribe to \emph{pointcuts}, representing areas of interest in Java classes, such as beginning of method, instruction before method call or specific bytecode instruction locations.
%
The Java classes are then parsed and fully visited, emitting an event on each instruction and other interesting locations.
%
Once an event is fired, the user code will be executed, in order to add instructions, remove others or compute bytecode-related features.

This simple model permit to include or remove new code automatically, using simple but powerful Java programs, named \emph{transformers}.
%
Transformers are usable on any Java programs, as long as these programs are processable by the ASM~\cite{asm} library.
%
BISM permits to instrument classes either on the fly, or statically before distribution.

\begin{figure}

		\begin{subfigure}[b]{0.45\textwidth}
			\includegraphics[width=\textwidth]{bism-loop.pdf}
			\caption{BISM Instrumentation loop with events one can subscribe to.}
			\label{fig:bism-loop}
		\end{subfigure}
		\begin{subfigure}[b]{0.45\textwidth}
			\includegraphics[width=1.1\textwidth]{bism-overview.pdf}
			\caption{A simple overview of BISM workflow.}
			\label{fig:bism-workflow}
		\end{subfigure}
\end{figure}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Mitigation Techniques Using BISM}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
In this section we briefly explain how, with a BISM transformer, we are capable of mitigating the \emph{Log4Shell} vulnerability.
%
We propose two mitigation techniques.
%
\begin{itemize}
	\item One which silently disable the \emph{JNDI} feature if used and let the variable substitution capability with other patterns.
	\item Another, completely disabling the variable substitution feature, if used without obfuscation.
\end{itemize}

\subsection{Mitigation 1: Silently Disabling JNDI}

Using BISM, we have the capability to inject any bytecode into any method.
%
Moreover, if we look at a stack trace from a vulnerable application, we can isolate the JNDI feature into a single method (\lstinline|lookup|) and a single class
 (\emph{org/apache/logging/log4j/core/lookup/JndiLookup}).
%
Therefore, if we are able to neutralize the code of this function, we disable the problematic \emph{JNDI} facility, letting all others variable substitution intact.

This is achieved by the transformer in Listing~\ref{lst:no-jndi}(slightly different than the real one, for ease of reading).
%
The \lstinline|insert| method permit to add bytecode at the location specified by the \emph{pointcut} (here it is at the beginning of the method).
%
The added code permits to add the following Java instruction: \lstinline[language=java]|return null;| which completely disable the functionality, has the vulnerable code became inaccessible.

\begin{lstlisting}[style=JavaStyle,label=lst:no-jndi,caption=A transformer removing the JNDI capability only., frame=single,float]
public void onMethodEnter(Method m, MethodDynamicContext dc) {
	//Check if we are in the right class
	if ( m.className.contains("org/apache/logging/log4j/core/lookup/JndiLookup") 
		 && m.name.equals("lookup")) {
		//Insert a "return null;" at beginning, the rest of the function 
		//therefore became dead code
		insert(new InsnNode(Opcodes.ACONST_NULL));
		insert(new InsnNode(Opcodes.ARETURN));
	}
}
\end{lstlisting}

\subsection{Disabling the variable substitution}

In order to protect programs, we can also modify them directly, instead of modifying the underlying library.

In this mitigation, we target any \lstinline|log/info/debug/...| call from a \emph{Logger} object with a specific signature: \lstinline|method(String message)|.
%
We then test if the logged string (\lstinline|message|) contains a non-obfuscated variable substitution pattern \lstinline|"${"|.
%
If the pattern is present, the logging method will not be executed, else it is.

This mitigation is directly injected into the application classes and make use of BISM ability to add new variables and modifying the control flow.
%
It is demonstrated in Listing~\ref{lst:remove-logger} and make use of two events, targeting the locations before and after any method call.

\begin{lstlisting}[style=JavaStyle,label=lst:remove-logger,caption=A transformer removing any suspect call to log methods.]
LabelNode not_execute;
Instruction to_jump;

private void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {
	//if we are calling a method from Logger, which gets a single string argument
	if (mc.methodOwner.contains("org/apache/logging/log4j/Logger")){
		if (mc.getNumberOfArgs() == 1 && Type.getArgumentTypes(mc.methodnode.desc)[0].equals( Type.getType(String.class))){
			//Retrieve the argument which is potentially infected
			DynamicValue str = dc.getMethodArgs(mc,1);
			//Inject bytecode : "if (!str.contains("${") { Log(str) }
			insert(ASMFactory.loadVar(str.type, str.index));
			insert(new LdcInsnNode("${"));
				insert(ASMFactory.invokeVirtual("java/lang/String", "contains", "(Ljava/lang/CharSequence;)Z"));
				not_execute = new LabelNode();
				insert(new JumpInsnNode(Opcodes.IFNE, not_execute));
				
				//Store the method call we modified, in order to add the post treatment in phase2
				to_jump = mc.ins;
			}
		}
	}
}
	
private void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {
	//If we applied a treatement to this method call
	if (mc.ins == to_jump) {
		LabelNode after_pop = new LabelNode();
		//if we just executed the Log, goto rest of function
		insert(new JumpInsnNode(Opcodes.GOTO, after_pop));
		//else, we have to clear the stack from Log argument and Logger instance
		insert(not_execute);
		insert(new InsnNode(Opcodes.POP2));
		//Normal function code begins here
		insert(after_pop);
	}
}

\end{lstlisting}

\subsection{Which Mitigation should be used?}

The choice between the two mitigation techniques depends on your acceptance degree of false positive.

If your application must never use variable substitution, the second mitigation is well-suited as it will completely disable the log when the message contains a substitution.
%
However, if you want to only disable the \emph{JNDI} capability, the first mitigation is better suited.

\section{User Manual to Protect Java Programs}

In order to use these two mitigation techniques, we distribute on the bism-public GitLab repository a Proof of Concept of our mitigation, on a sample vulnerable application~\cite{sample-app}.
%
One can follow the instructions in the bism-public repository, and detail here how to use the mitigation.

First of all, it is assumed that application that should be protected is launched by an instance of the Java Virtual Machine.
%
Moreover, the application should be launched through the Java command line.
%
Most of the time it is of the form:
\begin{center}
	\lstinline|java -jar <path/to/your/app.jar>|.
\end{center}
%
Moreover, three files are required:
\begin{itemize}
	\item \lstinline|bism.jar|, the library to instrument the application;
	\item \lstinline|Log4JMitigation.class|, the class that contains the transformer;
	\item \lstinline|config.xml|, the bism config file, to call the transformer on the application.
\end{itemize}

\emph{The first file has to be requested to us via the form in the repository}.\footnote{\url{https://framaforms.org/request-to-use-bism-1626425274}}

The two other files are retrievable from the repository, the class has to be compiled by BISM.
%
Instructions are available in the repository, in directory \lstinline|examples/log4shell-mitigation|.

One must place these three files in the same directory on your system, we suppose it is \lstinline|/bism/|.
%
Then, you must replace the java command-line to launch your application by:
\begin{center}
	\lstinline|java -javaagent:/bism/bism.jar=config=config.xml <the rest of your command line>| and, relaunch it.
\end{center}
%
To get more familiar with BISM, you can browse the repository and our paper in~\cite{bism}.


\bibliographystyle{IEEEtran}
\bibliography{biblio.bib}

\end{document}
