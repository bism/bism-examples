
This transformer offers two simple mitigations to the Log4Shell Attack (CVE-2021-44228)

This attack enables anyone to load a class on a vulnerable application.
To mitigate it, we replace the incrimined component (JndiLookup class, lookup method) by a stub one just performing
a return null.

This completely disable the log4j JNDI capability.
 
We propose two modes of mitigations:
 1. modifying the log4j class with a javaagent, which permits to remove only calls to JNDI and let all other substitions intact.
 2. removing calls to any method from the Logger class, with a single arg : String message_containing_variable_substitution
 
 To choose the mitigation (1), you either have to comment the non-needed call, or pass the argument "mitigation=Lookup"
 To choose the mitigation (2), comment the non needed call, or pass the argument "mitigation=Logger"
 
 Both arguments have to be passed through a bism config file.
 launch with : java -javaagent:<bism.jar location>=config=bism_config.xml <your java command line>
 
Your config file must be valid and contain at least these three entities for the mitigation (1):
* `<transformer value="Log4JMitigation"/>`
* `<scope value="org.apache.logging.log4j.core.lookup.JndiLookup.lookup"/>`
* `<transformer><arg key="mitigation" value="Lookup"/></transformer>`

For the second mitigation, you have to specify the target, modify the scope to include your vulnerable classes and change mitigation value to "Logger".

