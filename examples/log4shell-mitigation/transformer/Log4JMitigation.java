import inria.bism.transformers.ASMFactory;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.dynamiccontext.MethodDynamicContext;
import inria.bism.transformers.staticcontext.Method;
import inria.bism.transformers.staticcontext.MethodCall;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.*;
import inria.bism.transformers.staticcontext.Instruction;
import inria.bism.transformers.dynamiccontext.DynamicValue;

public class Log4JMitigation extends Transformer {

    //If we want to launch as an agent to disable JndiLookup, without modifying stored sw
    //
    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {
        if (arguments.containsKey("mitigation") && arguments.get("mitigation").equalsIgnoreCase("Lookup")) {
            insertJNDILookup(m);
        }
    }

    @Override
    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {
        if (arguments.containsKey("mitigation") && arguments.get("mitigation").equalsIgnoreCase("Logger")){
            removeLoggerCallPhase1(mc,dc);
        }
    }

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {
        if (arguments.containsKey("mitigation") && arguments.get("mitigation").equalsIgnoreCase("Logger")){
            removeLoggerCallPhase2(mc,dc);
        }
    }

    private void insertJNDILookup(Method m) {
        //Check if we are in the right class, in case the user did not provide a scope
        if (m.className.contains("org/apache/logging/log4j/core/lookup/JndiLookup") && m.name.equals("lookup")) {
            //Insert a "return null;" at beginning, the rest of the function therefore became dead code
            insert(new InsnNode(Opcodes.ACONST_NULL));
            insert(new InsnNode(Opcodes.ARETURN));
        }
    }


    LabelNode not_execute;
    Instruction to_jump;
    private void removeLoggerCallPhase1(MethodCall mc, MethodCallDynamicContext dc) {
        //if we are calling a method from Logger, which gets a single string argument
        if (mc.methodOwner.contains("org/apache/logging/log4j/Logger")){
            if (mc.getNumberOfArgs() == 1 && Type.getArgumentTypes(mc.methodNode.desc)[0].equals(Type.getType(String.class))){
                //Retrieve the argument which is potentially infected
                DynamicValue str = dc.getMethodArgs(mc,1);
                //Inject bytecode : "if (!str.contains("${") { Log(str) }
                insert(ASMFactory.loadVar(str.type, str.index));
                insert(new LdcInsnNode("${"));
                insert(ASMFactory.invokeVirtual("java/lang/String", "contains", "(Ljava/lang/CharSequence;)Z"));
                not_execute = new LabelNode();
                insert(new JumpInsnNode(Opcodes.IFNE, not_execute));

                //Store the method call we modified, in order to add the post treatment in phase2
                to_jump = mc.ins;
            }
        }
    }

    private void removeLoggerCallPhase2(MethodCall mc, MethodCallDynamicContext dc) {
        //If we applied a treatement to this method call
        if (mc.ins == to_jump) {
            LabelNode after_pop = new LabelNode();
            //if we just executed the Log, goto rest of function
            insert(new JumpInsnNode(Opcodes.GOTO, after_pop));
            //else, we have to clear the stack from Log argument and Logger instance
            insert(not_execute);
            insert(new InsnNode(Opcodes.POP2));
            //Normal function code begins here
            insert(after_pop);
        }
    }
}