# BISM Mitigation of log4shell

Assuming you have a jar you want to instrument named **app.jar** you have to :

* Put it into the *app* directory (and removing the present one)
* Launch `ant run` to execute the application with bism as javaagent which will prevent the execution of Log4Shell.

Alternatively, it is possible to launch `ant build` to get the transformer class and then launch `java -javaagent:<path/to/bism.jar>=config=bism.xml -jar <path/to/your/jar>` from this directory.
