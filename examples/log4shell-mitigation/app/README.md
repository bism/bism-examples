A sample vulnerable application which can be altered by the log4Shell weakness.

Retrieved from: https://github.com/christophetd/log4shell-vulnerable-app
