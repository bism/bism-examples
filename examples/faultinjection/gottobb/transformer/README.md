  A transformer which will inject faults on if statements, according to a point where the users
  want to go. This is an attacker with multiple faults model, which can determine precisely at
  which jump it is at every moment and could attack the flag register. Also it has a total view of
  the CFG.

