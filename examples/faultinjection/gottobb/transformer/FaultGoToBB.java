import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.InstructionDynamicContext;
import inria.bism.transformers.dynamiccontext.MethodDynamicContext;
import inria.bism.transformers.staticcontext.BasicBlock;
import inria.bism.transformers.staticcontext.Method;
import inria.bism.transformers.staticcontext.StaticContextProvider;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A transformer which will inject faults on if statements, according to a point where the users
 * want to go. This is an attacker with multiple faults model, which can determine precisely at
 * which jump it is at every moment and could attack the flag register. Also it has a total view of
 * the CFG.
 */
public class FaultGoToBB extends Transformer {

    BasicBlock destination;
    List<Integer> shortestPath;
    int current = 0;

    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {
        destination = chooseDestination(m);
        shortestPath = computeShortestPath(m, m.getEntryBlock(), destination);
        current = 0;
    }

    private List<Integer> computeShortestPath(Method m, BasicBlock src, BasicBlock destination) {
        Set<BasicBlock> visited = new HashSet<>();
        Set<BasicBlock> all = computeAllBasicBlocks(m);
        List<Integer> path = new LinkedList<>();
        BasicBlock[] prec = new BasicBlock[m.getNumberOfBasicBlocks()];
        Integer[] d = new Integer[m.getNumberOfBasicBlocks()];
        Arrays.fill(d, Integer.MAX_VALUE);
        d[0] = 0;
        while (visited.size() != m.getNumberOfBasicBlocks()) {
            BasicBlock a = minDistanceNotVisited(visited, d, all, m);
            visited.add(a);
            for (BasicBlock b : a.getSuccessorBlocks().stream().filter(bb -> !visited.contains(bb))
                    .collect(Collectors.toSet())) {
                if (d[b.id - src.id] > d[a.id - src.id] + 1) {
                    d[b.id - src.id] = d[a.id - src.id] + 1;
                    prec[b.id - src.id] = a;
                }
            }
        }
        BasicBlock bb = destination;
        while (bb != src) {
            path.add(0, bb.id);
            bb = prec[bb.id - src.id];
        }
        path.add(0, src.id);
        return path;
    }

    private Set<BasicBlock> computeAllBasicBlocks(Method m) {
        Set<BasicBlock> allBB = new HashSet<>();
        List<BasicBlock> toVisit = new ArrayList<>();
        toVisit.add(m.getEntryBlock());
        while (allBB.size() != m.getNumberOfBasicBlocks()) {
            BasicBlock cur = toVisit.remove(0);
            allBB.add(cur);
            toVisit.addAll(cur.getSuccessorBlocks().stream()
                    .filter(bb -> !allBB.contains(bb) && !toVisit.contains(bb))
                    .collect(Collectors.toSet()));
        }
        return allBB;
    }

    private BasicBlock minDistanceNotVisited(Set<BasicBlock> visited, Integer[] d,
            Set<BasicBlock> all, Method m) {
        int minDist = Integer.MAX_VALUE;
        BasicBlock minBB = null;
        for (BasicBlock bb : all.stream().filter(bb -> !visited.contains(bb))
                .collect(Collectors.toSet())) {
            if (d[bb.id - m.getEntryBlock().id] <= minDist) {
                minDist = d[bb.id - m.getEntryBlock().id];
                minBB = bb;
            }
        }
        return minBB;
    }

    private BasicBlock chooseDestination(Method m) {
        System.out.println("Method " + m.name
                + " : Choose which BB where to go (see visualize out to get block id)");
        Scanner sc = new Scanner(System.in);
        int bid = sc.nextInt();
        sc.close();
        for (BasicBlock b : m.getBasicBlocks()) {
            if (b.id == bid && b.method.name.equals(m.name))
                return b;
        }
        return null;
    }

    @Override
    public void onBasicBlockExit(BasicBlock bb, InstructionDynamicContext dc) {
        if (bb.id != shortestPath.get(current) || current == shortestPath.size() - 1)
            return;
        if (bb.getLastRealInstruction().isBranchingInstruction()) {
            Integer nextBB = shortestPath.get(current + 1);
            if (bb.getLastRealInstruction().isConditionalJump()) {
                for (int i = 0; i < bb.getLastRealInstruction()
                        .stackOperandsCountIfConditionalJump(); i++)
                    insert(new InsnNode(Opcodes.POP));
                if (bb.getFalseBranch().id == nextBB)
                    insert(new JumpInsnNode(Opcodes.GOTO,
                            (LabelNode) bb.getFalseBranch().getFirstInstruction()));
                else
                    insert(new JumpInsnNode(Opcodes.GOTO,
                            (LabelNode) bb.getTrueBranch().getFirstInstruction()));
            } else {
                insert(new InsnNode(Opcodes.POP));
                insert(new JumpInsnNode(Opcodes.GOTO,
                        (LabelNode) bb.getSuccessorBlocks().stream()
                                .filter(succ -> succ.id == nextBB).findFirst().get()
                                .getFirstInstruction()));
            }
            remove(bb.getLastRealInstruction());
        }
        current++;
    }
}
