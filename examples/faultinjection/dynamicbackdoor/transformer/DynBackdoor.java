import inria.bism.cfg.BlockType;
import inria.bism.transformers.ASMFactory;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.InstructionDynamicContext;
import inria.bism.transformers.dynamiccontext.MethodDynamicContext;
import inria.bism.transformers.staticcontext.*;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * A transformer which enable faults in certain if, according to the presence of a dynamic
 * "backdoor" in the system Here the backdoor is represented by the existence of an ip associated
 * with a given hostname
 */
public class DynBackdoor extends Transformer {

    String hostname = "no.thing";
    private Set<Integer> toInvert = new HashSet<>();
    MethodNode testAliveness =
            new MethodNode(Opcodes.ACC_PUBLIC | Opcodes.ACC_STATIC, "testAlive", "()Z", null, null);


    @Override
    public void onClassEnter(ClassContext c) {
        // Add a method which returns true if a given host is alive, false else.
        c.classNode.methods.add(testAliveness);
        LabelNode try_begin = new LabelNode();
        LabelNode try_end = new LabelNode();
        LabelNode catch_begin = new LabelNode();
        testAliveness.tryCatchBlocks
                .add(new TryCatchBlockNode(try_begin, try_end, catch_begin, null));
        testAliveness.instructions.add(try_begin);
        testAliveness.instructions.add(new LdcInsnNode(hostname));
        testAliveness.instructions.add(ASMFactory.invokeStatic("java/net/InetAddress", "getByName",
                "(Ljava/lang/String;)Ljava/net/InetAddress;"));
        testAliveness.instructions.add(new InsnNode(Opcodes.POP));
        testAliveness.instructions.add(new InsnNode(Opcodes.ICONST_1));
        testAliveness.instructions.add(new InsnNode(Opcodes.IRETURN));
        testAliveness.instructions.add(try_end);
        testAliveness.instructions.add(catch_begin);
        testAliveness.instructions.add(new InsnNode(Opcodes.POP));
        testAliveness.instructions.add(new InsnNode(Opcodes.ICONST_0));
        testAliveness.instructions.add(new InsnNode(Opcodes.IRETURN));
    }

    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {
        chooseDestination(m);
        insert(new LdcInsnNode("networkaddress.cache.ttl"));
        insert(new LdcInsnNode("1"));
        insert(ASMFactory.invokeStatic("java/security/Security", "setProperty",
                "(Ljava/lang/String;Ljava/lang/String;)V"));
    }

    LabelNode l_t, l_f;

    @Override
    public void onBasicBlockEnter(BasicBlock bb, InstructionDynamicContext dc) {
        if (bb.blockType == BlockType.CONDJUMP && toInvert.contains(bb.id)) {
            l_t = new LabelNode();
            l_f = new LabelNode();
        }
    }

    @Override
    public void onTrueBranchEnter(BasicBlock jumpingBlock, InstructionDynamicContext dc) {
        if (toInvert.contains(jumpingBlock.id)) {
            List<AbstractInsnNode> toAdd = new ArrayList<>();
            testBackdoor(toAdd, l_t, jumpingBlock);
            insert(toAdd);
            insert(new JumpInsnNode(Opcodes.GOTO, l_f));
            insert(l_t);
        }
    }

    @Override
    public void onFalseBranchEnter(BasicBlock jumpingBlock, InstructionDynamicContext dc) {
        if (toInvert.contains(jumpingBlock.id)) {
            List<AbstractInsnNode> toAdd = new ArrayList<>();
            testBackdoor(toAdd, l_f, jumpingBlock);
            insert(toAdd);
            insert(new JumpInsnNode(Opcodes.GOTO, l_t));
            insert(l_f);
        }
    }

    private void testBackdoor(List<AbstractInsnNode> toAdd, LabelNode real_if, BasicBlock bb) {
        toAdd.add(ASMFactory.invokeStatic(bb.method.className, testAliveness.name,
                testAliveness.desc));
        // If host is not alive, goto real branch
        toAdd.add(new JumpInsnNode(Opcodes.IFEQ, real_if));
    }

    private void chooseDestination(Method m) {
        System.out.println("Method " + m.name
                + " : Choose which BB(s) (1,2,3...) to set if (see visualize out to get block id)");
        Set<Integer> bids = new HashSet<>();
        int i = 0;

        try (Scanner sc = new Scanner(System.in)) {
            bids = Arrays.stream(sc.next().split(",")).map(Integer::parseInt)
                    .collect(Collectors.toSet());
        } catch (Exception e) {
            for (BasicBlock b : m.getBasicBlocks()) {
                if (b.blockType == BlockType.CONDJUMP && b.method.name.equals(m.name))
                    toInvert.add(b.id);
            }
            return;
        }
        for (BasicBlock b : m.getBasicBlocks()) {
            if (bids.contains(b.id) && b.blockType == BlockType.CONDJUMP
                    && b.method.name.equals(m.name))
                toInvert.add(b.id);
        }
    }
}
