  A transformer which enable faults in certain if, according to the presence of a "backdoor" in the
  system Here the backdoor is represented by the system properties -Dfault=XXXX..., by default if
  fault are enabled with -Dfault=1111... all ifs will be inverted (true and false branches are
  reverted). The format of the fault property is simply -Dfault=(<1 to invert if in ith Basic
  Block| 0 else>) with the basic block ordered like when you choose them at execution of the
  transformer. Example : If you choose to possibly invert ifs in Basic blocks : "1, 17, 23" and at
  execution you only want to invert the if in 23, you will have -Dfault=001
 Evaluate real if
 False branch of real if
 Here fault is globally activated, test it locally
 If fault is activated, we go to real true branch
 else we go to real false branch (no changing)
 True branch of real if
 If fault is activated, we go to real false branch
 else we go to real true branch (no changing)
 Here fault is globally deactivated, so just execute real if
 Just do nothing, the users wants to instrument everything

