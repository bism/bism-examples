import inria.bism.cfg.BlockType;
import inria.bism.transformers.ASMFactory;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.*;
import inria.bism.transformers.staticcontext.*;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A transformer which enable faults in certain if by setting their resulting values, according to
 * the presence of a "backdoor" in the system Here the backdoor is represented by the system
 * properties -Dfault=XXX. For example if you choose to set the ifs "1,17,23", and you use
 * -Dfault=021, the if at BB1 will jump to false branch, the if at BB17 will evaluate its stack and
 * BB23 will jump to true branch.
 */
public class SetIfs extends Transformer {

    LocalVariable la;
    String faultProperty = "fault";
    private HashMap<Integer, Integer> mapIdToIndex;

    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {
        chooseDestination(m);
        la = dc.addLocalVariable(m, "");
        testBackdoorExistence();
    }

    @Override
    public void onBasicBlockExit(BasicBlock bb, InstructionDynamicContext dc) {
        if (bb.blockType != BlockType.CONDJUMP || !mapIdToIndex.containsKey(bb.id))
            return;

        Instruction last = bb.getLastRealInstruction();
        List<AbstractInsnNode> toAdd = new ArrayList<>();
        LabelNode real_if = (new LabelNode());
        LabelNode false_branch = (new LabelNode()); // 0 -> false_branch pop;
        LabelNode true_branch = (new LabelNode()); // 2 -> true_branch pop;

        testBackdoor(toAdd, bb.id, real_if);
        toAdd.add(new TableSwitchInsnNode('0', '1', real_if, false_branch, true_branch));
        toAdd.add(false_branch);
        for (int i = 0; i < last.stackOperandsCountIfConditionalJump(); i++) {
            toAdd.add(new InsnNode(Opcodes.POP));
        }
        toAdd.add(new JumpInsnNode(Opcodes.GOTO,
                (LabelNode) bb.getFalseBranch().getFirstInstruction()));
        toAdd.add(true_branch);
        for (int i = 0; i < last.stackOperandsCountIfConditionalJump(); i++) {
            toAdd.add(new InsnNode(Opcodes.POP));
        }
        toAdd.add(new JumpInsnNode(Opcodes.GOTO,
                (LabelNode) bb.getTrueBranch().getFirstInstruction()));
        toAdd.add(real_if);
        bb.insertNextToInstruction(last.node, toAdd, true);
        bb.method.methodNode.maxStack++;
    }

    private void testBackdoor(List<AbstractInsnNode> toAdd, int bbId, LabelNode real_if) {
        toAdd.add(ASMFactory.loadVar(la.type, la.index));
        toAdd.add(new JumpInsnNode(Opcodes.IFNULL, real_if));
        toAdd.add(ASMFactory.loadVar(la.type, la.index));
        toAdd.add(new LdcInsnNode(mapIdToIndex.get(bbId)));
        toAdd.add(ASMFactory.invokeVirtual("java/lang/String", "charAt",
                Type.getMethodDescriptor(Type.CHAR_TYPE, Type.INT_TYPE)));
    }

    private void testBackdoorExistence() {
        insert(new LdcInsnNode(faultProperty));
        insert(ASMFactory.invokeStatic("java/lang/System", "getProperty",
                Type.getMethodDescriptor(Type.getType(String.class), Type.getType(String.class))));
        insert(ASMFactory.storeVar(la.type, la.index));
    }

    private void chooseDestination(Method m) {
        System.out.println("Method " + m.name
                + " : Choose which BB(s) (1,2,3...) to set if (see visualize out to get block id)");
        Set<Integer> bids = new HashSet<>();
        mapIdToIndex = new HashMap<>();

        try (Scanner sc = new Scanner(System.in)) {
            bids = Arrays.stream(sc.next().split(",")).map(Integer::parseInt)
                    .collect(Collectors.toSet());
        } catch (Exception e) {
            // Just do nothing, the users wants to instrument everything
        }
        int i = 0;
        if (bids.isEmpty()) {
            for (BasicBlock b : m.getBasicBlocks()) {
                if (b.blockType == BlockType.CONDJUMP && b.method.name.equals(m.name))
                    mapIdToIndex.put(b.id, i++);
            }
        } else {
            for (BasicBlock b : m.getBasicBlocks()) {
                if (bids.contains(b.id) && b.blockType == BlockType.CONDJUMP
                        && b.method.name.equals(m.name))
                    mapIdToIndex.put(b.id, i++);
            }
        }
    }
}
