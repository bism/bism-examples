import inria.bism.cfg.BlockType;
import inria.bism.transformers.ASMFactory;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.InstructionDynamicContext;
import inria.bism.transformers.dynamiccontext.LocalVariable;
import inria.bism.transformers.dynamiccontext.MethodDynamicContext;
import inria.bism.transformers.staticcontext.BasicBlock;
import inria.bism.transformers.staticcontext.Instruction;
import inria.bism.transformers.staticcontext.Method;
import inria.bism.transformers.staticcontext.StaticContextProvider;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.LdcInsnNode;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A transformer which enable faults in certain if, according to the presence of a "backdoor" in the
 * system Here the backdoor is represented by the system properties -Dfault=XXXX..., by default if
 * fault are enabled with -Dfault=1111... all ifs will be inverted (true and false branches are
 * reverted). The format of the fault property is simply -Dfault=(<1 to invert if in ith Basic
 * Block| 0 else>)* with the basic block ordered like when you choose them at execution of the
 * transformer. Example : If you choose to possibly invert ifs in Basic blocks : "1, 17, 23" and at
 * execution you only want to invert the if in 23, you will have -Dfault=001
 */
public class InvertIfs extends Transformer {

    LocalVariable la;
    String faultProperty = "fault";
    private HashMap<Integer, Integer> mapIdToIndex;

    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {
        chooseDestination(m);
        la = dc.addLocalVariable(m, "");
        testBackdoorExistence();
    }

    @Override
    public void onBasicBlockExit(BasicBlock bb, InstructionDynamicContext dc) {
        if (bb.blockType != BlockType.CONDJUMP || !mapIdToIndex.containsKey(bb.id))
            return;

        Instruction last = bb.getLastRealInstruction();
        LabelNode t_branch = (LabelNode) bb.getTrueBranch().getFirst().node;
        LabelNode f_branch = (LabelNode) bb.getFalseBranch().getFirst().node;
        LabelNode faulty_t_branch = new LabelNode();
        List<AbstractInsnNode> toAdd = new ArrayList<>();

        // Evaluate real if
        toAdd.add(new JumpInsnNode(last.opcode, faulty_t_branch));

        // False branch of real if
        testBackdoor(toAdd, bb.id, f_branch);
        // Here fault is globally activated, test it locally

        // If fault is activated, we go to real true branch
        toAdd.add(new JumpInsnNode(Opcodes.IF_ICMPEQ, t_branch));
        // else we go to real false branch (no changing)
        toAdd.add(new JumpInsnNode(Opcodes.GOTO, f_branch));

        // True branch of real if
        toAdd.add(faulty_t_branch);
        testBackdoor(toAdd, bb.id, t_branch);
        // If fault is activated, we go to real false branch
        toAdd.add(new JumpInsnNode(Opcodes.IF_ICMPEQ, f_branch));
        // else we go to real true branch (no changing)
        toAdd.add(new JumpInsnNode(Opcodes.GOTO, t_branch));
        // Here fault is globally deactivated, so just execute real if
        bb.insertNextToInstruction(last.node, toAdd, true);
        bb.removeInstruction(last);
    }

    private void testBackdoor(List<AbstractInsnNode> toAdd, int bbId, LabelNode real_if) {
        toAdd.add(ASMFactory.loadVar(la.type, la.index));
        toAdd.add(new JumpInsnNode(Opcodes.IFNULL, real_if));
        toAdd.add(ASMFactory.loadVar(la.type, la.index));
        toAdd.add(new LdcInsnNode(mapIdToIndex.get(bbId)));
        toAdd.add(ASMFactory.invokeVirtual("java/lang/String", "charAt",
                Type.getMethodDescriptor(Type.CHAR_TYPE, Type.INT_TYPE)));
        toAdd.add(new LdcInsnNode('1'));
    }


    private void testBackdoorExistence() {
        insert(new LdcInsnNode(faultProperty));
        insert(ASMFactory.invokeStatic("java/lang/System", "getProperty",
                Type.getMethodDescriptor(Type.getType(String.class), Type.getType(String.class))));
        insert(ASMFactory.storeVar(la.type, la.index));
    }

    private void chooseDestination(Method m) {
        System.out.println("Method " + m.name
                + " : Choose which BB(s) (1,2,3...) to set if (see visualize out to get block id)");
        Set<Integer> bids = new HashSet<>();
        mapIdToIndex = new HashMap<>();

        try (Scanner sc = new Scanner(System.in)) {
            bids = Arrays.stream(sc.next().split(",")).map(Integer::parseInt)
                    .collect(Collectors.toSet());
        } catch (Exception e) {
            // Just do nothing, the users wants to instrument everything
        }
        int i = 0;
        if (bids.isEmpty()) {
            for (BasicBlock b : m.getBasicBlocks()) {
                if (b.blockType == BlockType.CONDJUMP && b.method.name.equals(m.name))
                    mapIdToIndex.put(b.id, i++);
            }
        } else {
            for (BasicBlock b : m.getBasicBlocks()) {
                if (bids.contains(b.id) && b.blockType == BlockType.CONDJUMP
                        && b.method.name.equals(m.name))
                    mapIdToIndex.put(b.id, i++);
            }
        }
    }


}
