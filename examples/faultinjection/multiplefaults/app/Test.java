public class Test {
    public static void main(String args[]) {
        String code = "0000";
        String entered = "1111";

        if (args.length > 0) {
            entered = args[0].substring(0, 4);
        }

        boolean isAuth = true;
        for (int i = 0; i < code.length(); i++) {
            if (code.charAt(i) != entered.charAt(i)) {
                isAuth = false;
            }
        }

        if (isAuth) {
            System.out.println("Effectively auth with code : " + entered + " vs " + code);
         
        } else {
            System.out.println("Not auth with code : " + entered + " vs " + code);
        }
    }
}
