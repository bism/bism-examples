### Fault Injection

This folder contains examples on how to monitor and enforce Test Inversion and Arbitrary Jump attacks.

It also contains examples on how to simulate fault attacks dynamically and statically with BISM.


## ```How to run```
After obtaining bism.jar by [requesting](https://framaforms.org/request-to-use-bism-1626425274) the binaries of BISM.

You need to first place bism.jar in ../binaries directory in order to run the examples.

You need [ant](https://ant.apache.org) installed on your machine.

Navigate to each example and from the terminal.

To run:

``` 
ant run
```

To modify and build an example:
``` 
ant build
```

To clean:
``` 
ant clean
```


You can change BISM args by changing their values in the build.xml in each folder.

To change the location of the BISM jar, change the **binaries** property in build.xml
