import inria.bism.cfg.BlockType;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.InstructionDynamicContext;
import inria.bism.transformers.staticcontext.BasicBlock;
import inria.bism.transformers.staticcontext.Instruction;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;

/*
 * This transformer permit to go to the good branches after detecting a test inversion. It is
 * reapplicable so that multiple faults will be handled, just compose it with itself as many times
 * as you have faults
 */
public class TestInversionInlineEnforcer extends Transformer {

    LabelNode l_t;
    LabelNode l_f;

    @Override
    public void beforeInstruction(Instruction ins, InstructionDynamicContext dc) {
        if (ins.isConditionalJump()) {
            if (ins.stackOperandsCountIfConditionalJump() == 1) {
                this.insert(new InsnNode(Opcodes.DUP));
            } else {
                this.insert(new InsnNode(Opcodes.DUP2));
            }
        }

    }

    @Override
    public void onBasicBlockEnter(BasicBlock bb, InstructionDynamicContext dc) {
        if (bb.blockType == BlockType.CONDJUMP) {
            l_t = new LabelNode();
            l_f = new LabelNode();
        }
    }

    @Override
    public void onTrueBranchEnter(BasicBlock jumpingBlock, InstructionDynamicContext dc) {
        this.insert(new JumpInsnNode(jumpingBlock.getLastRealInstruction().opcode, l_t));
        this.insert(new JumpInsnNode(167, l_f));
        this.insert(l_t);
    }

    @Override
    public void onFalseBranchEnter(BasicBlock jumpingBlock, InstructionDynamicContext dc) {
        this.insert(new JumpInsnNode(jumpingBlock.getLastRealInstruction().opcode, l_t));
        this.insert(new JumpInsnNode(167, l_f)); // Symmetric but unnecessary
        this.insert(l_f);
    }
}
