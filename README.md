# BISM Examples Repository


In this repository, you will find examples of BISM transformers that demonstrate different BISM features and different use cases. More about BISM can be found in the [BISM repository](https://gitlab.inria.fr/bism/bism-public) and [BISM DSL](https://gitlab.inria.fr/bism/bism-dsl).

We discuss below the different folders in this repository.
 
### Main Folder (examples)

The folder `examples` contains examples that highlight BISM features and different use cases. 

These examples are written using BISM API is transformers are written in Java.

The folder `to-be-filled` contains a template that you can use to write your own transformer. You can find the following in this folder:
- app: contains source code or jar files of the program that you want to instrument.
- transformer: contains the transformer that you want to use to instrument the program.
- build.xml: contains a build file to help you build the example. Normally, this file does not need to be modified.
- bism.xml: contains the configuration file for BISM which includes the transformer, scope, visualization, and other BISM args.

### With Complex Event Processing (with-cep)

The folder `with-cep` contains examples that integrate BISM with BeepBeep. For more details, please refer to the [https://gitlab.inria.fr/bism/beepbeep-bism](https://gitlab.inria.fr/bism/beepbeep-bism).


These examples also include BISM transformers that are written using BISM DSL [https://gitlab.inria.fr/bism/bism-dsl](https://gitlab.inria.fr/bism/bism-dsl).

The folder `simple` contains a simple example that uses BISM DSL to write a transformer that prints the method name before calling it. Inside this folder, you will find the following structure:
- inst: contains the instrumentation code. Including the `config.xml` file which contains the configuration for BISM.

- processor: contains the processors that will be used to process the events.  
- src: contains the source code of the program
- adapter.properties: contains the configuration for the adapter which contains the processors and the processing mode: async or sync.
- build.xml: contains a build file to help you build the example.

### Libs Folder (libs)

This folder contains the libraries that are used by the examples including BISM and BeepBeep.

### How to run

Each of the examples can be run from the terminal using ant.

To install ant on your machine, please refer to [https://ant.apache.org](https://ant.apache.org).


Navigate to the example folder and run:

```bash
ant run
```

To clean the directory:

```bash
ant clean
```

To build the example:

```bash
ant build
```

To modify the example, you can change the code inside the files and then run the example again.


### Folder Structure

Below is the folder structure of the repository:

 
```markdown

|-- examples
|   |-- bismfeatures
|   |   |-- composition
|   |   |-- localArrays
|   |   |-- localVariables
|   |   `-- staticFields
|   |-- faultinjection
|   |   |-- dynamicbackdoor
|   |   |-- gottobb
|   |   |-- multiplefaults
|   |   |-- testinversiondetect
|   |   `-- testinversionenforce
|   |-- javagoodpractice
|   |   |-- HasNext
|   |   `-- UnSafeIterator
|   |-- log4shell-mitigation
|   |-- logging
|   |-- obfuscation
|   |   |-- controlflowflattening
|   |   |-- junkcode
|   |   |-- methodmerging
|   |   `-- syntaxicrenaming
|   |-- secrecy
|   |-- staticanalysis
|   `-- to-be-filled
|       |-- app
|       |-- transformer
|       |-- bism.xml
|       `-- build.xml
|-- libs
`-- with-cep
    |-- complex-events
    |-- monitoring
    |   `-- parametric
    |-- profiling
    |   `-- callgraph
    |-- simple
    |   |-- inst
    |   |-- processor
    |   `-- src
    `-- testing-coverage
        |-- branch
        `-- indirect
```
