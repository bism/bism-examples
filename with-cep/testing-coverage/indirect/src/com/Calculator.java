package com;

public class Calculator {
    public int add(int a, int b) {
        if(b == 0) {
            return a;
        }
        doNothing();
        return a + b;
    }

    public int subtract(int a, int b) {
        if(b == 0) {
            return a;
        }
        doNothing();
        return a - b;
    }

    public int multiply(int a, int b) {
        if(b == 0) {
            return 0;
        }
        doNothing();
        return a * b;
    }

    public int divide(int a, int b) {
        if(b == 0) {
            return 0;
        }
        return a / b;
    }
    public void doNothing() {
        
    }
}
