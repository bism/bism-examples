package com;

import org.junit.Assert;
import org.junit.Test;

public class FullTest {
    @Test
    public void testAdd() {
        Calculator calculator = new Calculator();
        Assert.assertEquals(5, calculator.add(3, 2));
        Assert.assertEquals(3, calculator.add(3, 0));
    }

    @Test
    public void testSubtract() {
        Calculator calculator = new Calculator();
        Assert.assertEquals(1, calculator.subtract(3, 2));
        Assert.assertEquals(3, calculator.subtract(3, 0));
    }

    @Test
    public void testMultiply() {
        Calculator calculator = new Calculator();
        Assert.assertEquals(6, calculator.multiply(3, 2));
        Assert.assertEquals(0, calculator.multiply(3, 0));
    }

    @Test
    public void testDivide() {
        Calculator calculator = new Calculator();
        Assert.assertEquals(1, calculator.divide(2, 2));
        Assert.assertEquals(0, calculator.divide(2, 0));
    }
}
