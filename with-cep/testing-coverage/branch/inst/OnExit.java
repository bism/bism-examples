import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.dynamiccontext.MethodDynamicContext;
import inria.bism.transformers.staticcontext.Method;
import inria.bism.transformers.*;

public class OnExit extends FastTransformer {

    final String owners = "owners";
    final String methods = "methods";

    @Override
    public void onMethodExit(Method m, MethodDynamicContext dc) {

        if (filterAccess(owners, methods, m.className, m.name, true))
            return;

        StaticInvocation sti =
                new StaticInvocation("bbadapter/Observer", "finish");
        invoke(sti);

    }
}
