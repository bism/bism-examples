import inria.bism.cfg.BlockType;
import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.*;
import inria.bism.transformers.staticcontext.BasicBlock;
import inria.bism.transformers.staticcontext.ClassContext;
import inria.bism.transformers.staticcontext.Method;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class BranchCoverageTransformer extends Transformer {


    LocalArray lkeys = null;
    LocalArray lvalues = null;


    @Override
    public void onClassEnter(ClassContext c) {
        for (MethodNode mn : c.classNode.methods) {

            c.addField(new FieldNode(Opcodes.ACC_PUBLIC | Opcodes.ACC_STATIC, mn.name.replace("<", "").replace(">", "")
                    + mn.desc.hashCode(), "Z", null, null));

        }
    }

    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {

        HashSet<String> cfg = new HashSet<>();
        for (BasicBlock b : m.getBasicBlocks()) {
            if (m.getEntryBlock() == b) {
                cfg.add("start" + "->" + b.id + "" + b.lines);
            }

            for (BasicBlock succ : b.getSuccessorBlocks()) {
                cfg.add(b.id + "" + b.lines + "->" + succ.id + "" + succ.lines);
            }

            if (m.getExitBlocks().contains(b)) {
                cfg.add(b.id + "" + b.lines + "->" + "end");
            }
        }

        String fieldName = m.methodNode.name.replace("<", "").replace(">", "") + m.methodNode.desc.hashCode();

        lastBlock = dc.addLocalVariable(m, "start");

        List<AbstractInsnNode> ins = new ArrayList<>();
        ins.add(new FieldInsnNode(Opcodes.GETSTATIC, m.className, fieldName, "Z"));
        LabelNode l_1 = new LabelNode();
        ins.add(new JumpInsnNode(Opcodes.IFNE, l_1));
        insert(ins);

        lkeys = dc.createLocalArray(m, String.class);
        lvalues = dc.createLocalArray(m, Object.class);
        StaticInvocation sti = new StaticInvocation("bbadapter/Observer", "observe");
        dc.addToLocalArray(lkeys, "thread");
        dc.addToLocalArray(lkeys, "class-name");
        dc.addToLocalArray(lkeys, "method-name");
        dc.addToLocalArray(lkeys, "method-args");
        dc.addToLocalArray(lkeys, "method-uuid");
        dc.addToLocalArray(lkeys, "method-edge");
        dc.addToLocalArray(lkeys, "method-cfg");
        dc.addToLocalArray(lvalues, dc.getThreadName(m));
        dc.addToLocalArray(lvalues, m.className.replace("/", ".").replace("com.", ""));
        dc.addToLocalArray(lvalues, m.name);
        dc.addToLocalArray(lvalues, m.methodNode.desc);
        dc.addToLocalArray(lvalues, dc.getRandomUUID(m));
        dc.addToLocalArray(lvalues, cfg.toString().replace("[", "").replace("]", ""));
        dc.addToLocalArray(lvalues, true);
        sti.addParameter(lkeys);
        sti.addParameter(lvalues);
        invoke(sti);

        ins = new ArrayList<>();
        ins.add(new InsnNode(Opcodes.ICONST_1));
        ins.add(new FieldInsnNode(Opcodes.PUTSTATIC, m.className, fieldName, "Z"));
        ins.add(l_1);
        insert(ins);

        lkeys = dc.createLocalArray(m, String.class);
        lvalues = dc.createLocalArray(m, Object.class);

    }


    LocalVariable lastBlock;

    @Override
    public void onBasicBlockEnter(BasicBlock bb, InstructionDynamicContext dc) {

        dc.clearLocalArray(bb.getFirst(), lkeys);
        dc.clearLocalArray(bb.getFirst(), lvalues);

        StaticInvocation sti = new StaticInvocation("bbadapter/Observer", "observe");

        dc.addToLocalArray(lkeys, "class-name");
        dc.addToLocalArray(lkeys, "method-name");
        dc.addToLocalArray(lkeys, "method-args");

        dc.addToLocalArray(lkeys, "from");
        dc.addToLocalArray(lkeys, "to");
//        dc.addToLocalArray(lkeys, "method-cfg");

        dc.addToLocalArray(lvalues, bb.method.className.replace("/", ".").replace("com.", ""));
        dc.addToLocalArray(lvalues, bb.method.name);
        dc.addToLocalArray(lvalues, bb.method.methodNode.desc);

        if (bb != bb.method.getEntryBlock()) {
            dc.addToLocalArray(lvalues, lastBlock);

        } else {
            dc.addToLocalArray(lvalues, "start");

        }

        dc.addToLocalArray(lvalues, bb.id + "" + bb.lines);
//        dc.addToLocalArray(lvalues, false);

        if (bb == bb.method.getEntryBlock()) {
            dc.addToLocalArray(lkeys, "method-enter");
            dc.addToLocalArray(lvalues, "1");

        }

        sti.addParameter(lkeys);
        sti.addParameter(lvalues);
        invoke(sti);

    }

    @Override
    public void onBasicBlockExit(BasicBlock bb, InstructionDynamicContext dc) {

        if (bb.blockType == BlockType.RETURN) {

            dc.clearLocalArray(bb.getFirst(), lkeys);
            dc.clearLocalArray(bb.getFirst(), lvalues);

            StaticInvocation sti = new StaticInvocation("bbadapter/Observer", "observe");

            dc.addToLocalArray(lkeys, "class-name");
            dc.addToLocalArray(lkeys, "method-name");
            dc.addToLocalArray(lkeys, "method-args");

            dc.addToLocalArray(lkeys, "from");
            dc.addToLocalArray(lkeys, "to");

//            dc.addToLocalArray(lkeys, "method-cfg");

            dc.addToLocalArray(lvalues, bb.method.className.replace("/", ".").replace("com.", ""));
            dc.addToLocalArray(lvalues, bb.method.name);
            dc.addToLocalArray(lvalues, bb.method.methodNode.desc);

            dc.addToLocalArray(lvalues, bb.id + "" + bb.lines);
            dc.addToLocalArray(lvalues, "end");

//            dc.addToLocalArray(lvalues, false);

            dc.addToLocalArray(lkeys, "method-exit");
            dc.addToLocalArray(lvalues, "1");

            sti.addParameter(lkeys);
            sti.addParameter(lvalues);
            invoke(sti);

        }
//
        dc.updateLocalVariable(bb.getFirstRealInstruction(), lastBlock, bb.id + "" + bb.lines);

    }

}
