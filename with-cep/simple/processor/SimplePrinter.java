
import bbadapter.processor.EventProcessor;

import ca.uqac.lif.cep.Processor;
import ca.uqac.lif.cep.io.Print;

/**
 * Simple prints the event using it toString method.
 */
public class SimplePrinter extends EventProcessor {
    @Override
    public Processor getPushableProcessor() {

        return new Print().setSeparator("\n");
    }
}
