import bbadapter.event.Event;
import bbadapter.processor.EventProcessor;

import ca.uqac.lif.cep.Connector;
import ca.uqac.lif.cep.Processor;
import ca.uqac.lif.cep.functions.ApplyFunction;
import ca.uqac.lif.cep.functions.FunctionLambda;
import ca.uqac.lif.cep.io.Print;



/***
 * Prints an event with a custom format.
 */
public class FancyPrinter extends EventProcessor {
    @Override
    public Processor getPushableProcessor() {

        FunctionLambda fl = new FunctionLambda((Object o) -> {
            Event e = (Event) o;
            StringBuilder sb = new StringBuilder();
            sb.append("Fancy Event: {\n");

            e.ctx.forEach((key, value) -> {
                sb.append("  ").append(key).append(": ").append(value).append("\n");
            });

            sb.append("}");
            return sb.toString();
        }).setReturnType(String.class);

        ApplyFunction fancyToString = new ApplyFunction(fl);

        Print p = new Print().setSeparator("\n");
        Connector.connect(fancyToString, p);

        return fancyToString;
    }
}
