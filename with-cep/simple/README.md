# Simple

This is a simple example where the program is instrumented to capture method calls and emit events, then two processors are defined to print those events.

## Folder Structure
- `inst` - contains instrumentation code
- `src` - contains the source code of the program
- `processors` - contains the processors that will be used to process the events
- `build.xml` - contains a build file to help you build the example
-  `adapter.properties` - contains the configuration for the adapter which contains the processors and the processing mode: async or sync.

 
