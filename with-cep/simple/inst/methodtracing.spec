pointcut pc1 before MethodCall(* *.*.*(..))

event e1(["Class", "Method Name", "Args"], [methodOwner,methodName,getAllMethodArgs]) on pc1

monitor m1{
    class: bbadapter.Observer
    events: [e1 to observe(List, List)]
}
