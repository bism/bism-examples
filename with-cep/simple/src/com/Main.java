package com;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Main {

    public static void main(String[] args) {
        printHello();

        calculateArea(5, 10);

        int maxNum = findMax(3, 8, 1);
        System.out.println("Maximum Number: " + maxNum);
    }

    public static void printHello() {
        System.out.println("Hello, World!");
    }


    public static void calculateArea(int length, int width) {
        int area = length * width;
    }


    public static int findMax(int a, int b, int c) {
        int max = a;
        if (b > max) {
            max = b;
        }
        if (c > max) {
            max = c;
        }
        return max;
    }

}
