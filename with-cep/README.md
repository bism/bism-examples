# Examples

This folder contains examples integrating BISM and BeepBeep.  

## Folder Structure
Inside each example folder you will find the following structure:

- `inst` - contains instrumentation code
- `src` - contains the source code of the program
- `processors?` - contains the processors that will be used to process the events. Some examples do not have this folder. These examples used processors from the palette we developed and can be found under [https://gitlab.inria.fr/bism/beepbeep-bism](https://gitlab.inria.fr/bism/beepbeep-bism). They are under `/src/analysis` from the root folder.
- `build.xml` - contains a build file to help you build the example
-  `adapter.properties` - contains the configuration for the adapter which contains the processors and the processing mode: async or sync.


## How to run the examples


This folder contains examples integrating BISM and BeepBeep  
 
To run the examples you need **ant** installed on your machine.

Then from inside each experiment directory.
```bash
cd example-name
```

To build
```bash
ant build
```

To run  
```bash
ant run
```

To clean the directory:
```bash
ant clean
```

