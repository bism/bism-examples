import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.*;
import inria.bism.transformers.staticcontext.BasicBlock;
import inria.bism.transformers.staticcontext.ClassContext;
import inria.bism.transformers.staticcontext.Method;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.VarInsnNode;

import java.util.HashSet;

public class ProfilingTransfromer extends Transformer {


    LocalArray myKeys = null;
    LocalArray myValues = null;
    DynamicValue timer = null;

    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {

        myKeys = dc.createLocalArray(m, String.class);
        myValues = dc.createLocalArray(m, Object.class);



        StaticInvocation sti = new StaticInvocation("bbadapter/Observer", "observe");


        dc.addToLocalArray(myKeys, "class-name");
        dc.addToLocalArray(myKeys, "method-name");
        dc.addToLocalArray(myKeys, "method-uuid");

    
        dc.addToLocalArray(myValues, m.className.replace("/", ".").replace("transactionsystem.", ""));
        dc.addToLocalArray(myValues, m.name);
        dc.addToLocalArray(myValues, dc.getRandomUUID(m));

        sti.addParameter(myKeys);
        sti.addParameter(myValues);

        invoke(sti);
//
        timer = dc.addLocalVariable(m, 0L);
        insert(new VarInsnNode(Opcodes.LLOAD, timer.index));
        StaticInvocation sysTime = new StaticInvocation("java/lang/System", "currentTimeMillis", "J");
        invoke(sysTime);
        insert(new InsnNode(Opcodes.LSUB));
        insert(new VarInsnNode(Opcodes.LSTORE, timer.index));
//

    }

    @Override
    public void onMethodExit(Method m, MethodDynamicContext dc) {

        insert(new VarInsnNode(Opcodes.LLOAD, timer.index));
        StaticInvocation sysTime = new StaticInvocation("java/lang/System", "currentTimeMillis", "J");
        invoke(sysTime);
        insert(new InsnNode(Opcodes.LADD));
        insert(new VarInsnNode(Opcodes.LSTORE, timer.index));
//

        StaticInvocation sti = new StaticInvocation("bbadapter/Observer", "observe");

        dc.addToLocalArray(myKeys, "method-exit");
        dc.addToLocalArray(myValues, "true");
        dc.addToLocalArray(myKeys, "time-spent");
        dc.addToLocalArray(myValues, timer);

        sti.addParameter(myKeys);
        sti.addParameter(myValues);

        invoke(sti);

    }




}
