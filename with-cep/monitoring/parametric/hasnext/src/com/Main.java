package com;

import java.util.*;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;

public class Main {

    public static void main(String[] args) {

        int j = args.length > 0 ? Integer.parseInt(args[0]) : 1;

        for (int i = 1; i <= j; i++) {
            runExperiment(i);
        }

    }


    public static void runExperiment(int j) {

        for (int i = 0; i <= j * 10; i++) {
            ListOperationsExample.execute();
        }

    }

}
