package com;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListOperationsExample {
    public static void execute() {
        // Create a list of integers
        List<Integer> numbers = new ArrayList<>();

        // Add elements to the list
        numbers.add(10);
        numbers.add(20);
        numbers.add(30);
        numbers.add(10);
        numbers.add(20);
        numbers.add(30);

        // Iterate over the list using an iterator
        Iterator<Integer> iterator = numbers.iterator();

	//Expected Violation here
	iterator.next();
	iterator.next();

        while (iterator.hasNext()) {
            int number = iterator.next();

        }

        // Remove an element using an iterator
        iterator = numbers.iterator();
        while (iterator.hasNext()) {
            int number = iterator.next();
            if (number == 20) {
                iterator.remove();
            }
        }

        // Check if the list contains a specific element
        boolean containsElement = numbers.contains(30);

        // Get the size of the list
        int size = numbers.size();

        // Clear the list
        numbers.clear();

    }
}
