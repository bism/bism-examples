pointcut pc1 before MethodCall(* *.*Iterator.next())

pointcut pc2 before MethodCall(* *.*Iterator.hasNext())

pointcut pc4 on MethodExit(* com.Main.main(..))

event e1(["name", "iterator"], ["n",getMethodReceiver]) on pc1

event e2(["name",  "iterator"], ["h",getMethodReceiver]) on pc2

event e4("end") on pc4 to bbadapter.Observer.finish(String)

monitor m1{
       class: bbadapter.Observer
    events: [e1 to observe(List, List), e2 to observe(List, List)
            ]
}
