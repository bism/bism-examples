pointcut pc1 before MethodCall(* *.Iterator.next())

pointcut pc2 after MethodCall(* *.List.iterator(..))

pointcut pc3 after MethodCall(* *.List.remove*(..)) || after MethodCall(* *.List.add*(..))

pointcut pc4 on MethodExit(* transactionsystem.Main.main(..))


event e1(["name", "iterator"], ["n",getMethodReceiver]) on pc1

event e2(["name", "list", "iterator"], ["c",getMethodReceiver,getMethodResult]) on pc2

event e3(["name", "list"], ["u",getMethodReceiver]) on pc3

event e4("end") on pc4 to bbadapter.Observer.finish(String)


monitor m1{
        class: bbadapter.Observer
    events: [e1 to observe(List, List), e2 to observe(List, List), e3 to observe(List, List)
                ]
}
