//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package transactionsystem;

public class UserAccount {
    protected boolean opened;
    protected String account_number;
    protected double balance;
    protected Integer owner;

    public UserAccount(Integer uid, String anumber) {
        this.account_number = anumber;
        this.balance = 0.0;
        this.opened = false;
        this.owner = uid;
    }

    public String getAccountNumber() {
        return this.account_number;
    }

    public double getBalance() {
        return this.balance;
    }

    public Integer getOwner() {
        return this.owner;
    }

    public void enableAccount() {
        this.opened = true;
    }

    public void closeAccount() {
        this.opened = false;
    }

    public void withdraw(double amount) {
        this.balance -= amount;
    }

    public void deposit(double amount) {
        this.balance += amount;
    }
}
