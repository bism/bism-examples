package transactionsystem;

import java.util.*;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;

public class Main {

    public static void main(String[] args) {

        System.out.println("Running all scenarios");
        Scenarios.runAllScenarios();
        System.out.println("Completed");

    }

}
