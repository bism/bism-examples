package com;

import java.util.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class Main {

    public static void main(String[] args) throws IOException {

        createFile("foo.txt", 100);  // Create a file with 100 random characters
        createFile("bar.txt", 200);  // Create a file with 200 random characters



        FileOperator operator = new FileOperator();

        operator.openFile("foo.txt");
        operator.readFile("foo.txt", 0, 10);
        operator.openFile("bar.txt");
        operator.readFile("foo.txt", 9, 12);
        operator.writeFile("bar.txt", 0, "Hello, world!");
        operator.writeFile("bar.txt", 33, "abc");
        operator.readFile("foo.txt", 12, 5);
        operator.closeFile("bar.txt");
        operator.closeFile("foo.txt");
        operator.openFile("bar.txt");
        operator.writeFile("bar.txt", 10, "Hello again!");
        operator.closeFile("bar.txt");
    }

    private static final String CHARACTERS = "abcdefghijklmnopqrstuvwxyz";

    private static String getRandomString(int length) {
        StringBuilder builder = new StringBuilder(length);
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            builder.append(CHARACTERS.charAt(random.nextInt(CHARACTERS.length())));
        }
        return builder.toString();
    }

    public static void createFile(String filename, int length) throws IOException {
        try (FileWriter writer = new FileWriter(filename)) {
            writer.write(getRandomString(length));
        }
    }
}
