package com;

import java.io.RandomAccessFile;
import java.io.IOException;
import java.util.HashMap;

public class FileOperator {

    public static HashMap<String, RandomAccessFile> files = new HashMap<String, RandomAccessFile>();

    public RandomAccessFile openFile(String filename) throws IOException {
        files.put(filename, new RandomAccessFile(filename, "rw"));
        return files.get(filename);
    }

    public void readFile(String filename, int offset, int length) throws IOException {
        RandomAccessFile file = files.get(filename);
        byte[] bytes = new byte[length];
        file.seek(offset);
        file.read(bytes, 0, length);

    }

    public void writeFile(String filename, int offset, String data) throws IOException {
        RandomAccessFile file = files.get(filename);
        file.seek(offset);
        file.write(data.getBytes());

    }

    public void closeFile(String filename) throws IOException {
        RandomAccessFile file = files.get(filename);
        file.close();
    }
}
