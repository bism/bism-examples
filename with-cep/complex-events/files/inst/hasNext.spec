pointcut pc1 after MethodCall(* *.FileOperator.openFile(..))
pointcut pc2 after MethodCall(* *.FileOperator.closeFile(..))
pointcut pc3 after MethodCall(* *.FileOperator.readFile(..))
pointcut pc4 after MethodCall(* *.FileOperator.writeFile(..))
pointcut pc5 on MethodExit(* com.Main.main(..))

event e1(["event", "args"], ["open",getAllMethodArgs]) on pc1
event e2(["event",  "args"], ["close",getAllMethodArgs]) on pc2
event e3(["event",  "args"], ["read",getAllMethodArgs]) on pc3
event e4(["event",  "args"], ["write",getAllMethodArgs]) on pc4
event e5("end") on pc5 to ca.uqac.lif.analysis..finish(String)

monitor m1{
    class: ca.uqac.lif.analysis.
    events: [   e1 to observe(List, List),
                e2 to observe(List, List),
                e3 to observe(List, List),
                e4 to observe(List, List)
            ]
}
