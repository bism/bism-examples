## File Event Aggregator

This example creates complex events by aggregating low-level events that occur to files. Each basic event represents an operation performed on a `File` and is represented as an array with at least two elements:

- The name of the file being manipulated (a String)
- The name of the operation being performed on this file (a String)

The available operations are `open`, `close`, `read`, or `write`. For `write` or `read` operations, the array contains two additional elements:

- The offset at which bytes are written (or read)
- The number of bytes being written (or read)

The objective is to generate a high-level stream where each event summarizes the "lifecycle" of a file. When a file is closed, a "complex" `FileOperation` event should be created, providing a summary of the operations performed on the file while it was open. The `FileOperation` event includes the following information:

- The filename
- Whether the interaction was for reading or writing
- The number of bytes read or written
- Whether the operations were contiguous. Contiguous operations mean that the first read (or write) operation occurs at the start of the file (offset 0), and each subsequent operation starts where the previous one left off.
- The global range of bytes accessed by this interaction. This range is calculated as the interval from the minimum value of the offset seen in an operation to the maximum value of the offset + length.

The pipeline also allows resets, which means that if the same file is reopened at a later time, a new complex event will eventually be generated for the new interaction.
