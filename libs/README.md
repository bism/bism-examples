# Libs

This folder contains the pre-built libraries used in the experiments and examples.

 - `analysis.jar` - contains the processors we developed for this work. They are under `/src/analysis` from the root folder.
 - `bbadapter.jar` - contains the adapter we developed for this work. It is under `/src/bbadapter` from the root folder.
 - `bism.jar` - contains the BISM library
 - `beebeep-3-0.10.9a.jar` - contains the BeepBeep library

 The remaining jars are dependencies of the libraries above.


